<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */

define('DB_NAME', 'realestate');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '6)tXK64Gr2Tfo+9UG7E`yeNfe$MT{EH*3xE K-v:QJ}>B(Zz>>BX/+<qO3.A8%+w');
define('SECURE_AUTH_KEY',  'qdTI_r,s/@ezR+cE93*[$VKmdb}W?V^G/[(jzbmq7*Uy>OT7^#Lp^k(`8I9:5{|y');
define('LOGGED_IN_KEY',    '-Aqi}m6lA1W#Ps}mqdz)@,<Io+L!zV-!~0c)KZP;j=e-Q0Oi/vlIrY&mH+boox/a');
define('NONCE_KEY',        'POE0O3u;9_TnKw96WJ$_eCb$<.8]p8sG:cCF5,7c(%!m2}~+k|kOxY1]H|5Wu+hn');
define('AUTH_SALT',        '^2v[+Vt!z(AV8E!IU~-$37oD5d1+VQ3}~q7 ~L*JTeE&F|=GY$R =9|~MxoFH|Pm');
define('SECURE_AUTH_SALT', '>sU41:<eDk5L8!c]]CT|8NhTMc2U>/RV~Ivl8&{Nr3ICVV>Rt^=Pa 2v*~~>Aw~B');
define('LOGGED_IN_SALT',   '+ m9|_horc^Av~M0[/oqp]G+-EFh{G>+hp?M+srE+? @ER2fqo>Za0SdY#7tgb<}');
define('NONCE_SALT',       'Y8NG*N[f,hH>FwbG.;(r.r:T|4Xi?n G[~BT_=l>{Ey),{[%,WQE)3igIh0pyaAD');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
