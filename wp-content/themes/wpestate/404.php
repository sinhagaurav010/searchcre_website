<?php
// Template Name: Advanced Results
// Wp Estate Pack

get_header();
$options=sidebar_orientation();
?>


<!-- Google Map Code -->
<?php 
//get_template_part('libs/templates/map-template'); 
?> 
<!-- Google Map Code -->





<div id="wrapper" class="<?php print $options['fullwhite']; ?>">  
    <div class="<?php print $options['add_back']; ?>"></div>

    <?php
    print breadcrumb_container($options['full_breadcrumbs'],$options['bread_align'] )
    ?>
    <div id="main" class="row <?php print $options['sidebar_status']; ?>">
    <?php
    print display_breadcrumbs( $options['full_breadcrumbs'] ,$options['bread_align_internal'] )
    ?>


    <!-- begin content--> 
    <div id="post" class=" blogborder <?php print $options['grid'].' ' .$options['shadow']; ?>"> 
        <div class="inside_post inside_no_border">

            <h1 class="entry-title"><?php _e('Page not found','wpestate');?></h1>
            <div>
            <?php _e( 'We\'re sorry. Your page could not be found.', 'wpestate' ); ?>
            </div>
          
        </div> <!-- end inside post-->
    </div>
    <!-- end content-->




       <?php  include(locate_template('customsidebar.php')); ?>


    </div><!-- #main -->    
</div><!-- #wrapper -->
<?php
get_footer();
?>