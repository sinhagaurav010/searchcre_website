<?php
// Template Name: Brokers list

get_header();
$options=sidebar_orientation($post->ID);
?>

<!-- Google Map Code -->
<?php 
//get_template_part('libs/templates/map-template'); 
?> 
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">


<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        

 <style>
 .modal-backdrop.in{z-index:1 !important;}
</style> 
<!-- Google Map Code -->
<div id="wrapper" class="<?php print $options['fullwhite']; ?>" style="background:#000;">  
    <div class="<?php print $options['add_back']; ?>"></div>

     <?php
    print breadcrumb_container($options['full_breadcrumbs'],$options['bread_align'] )
    ?>
    <div style="padding:0px;max-width:100% !important;" id="main" class="row <?php print $options['sidebar_status']; ?>">
			
     
        <!-- the div that represents the modal dialog -->
        <div class="modal fade" id="contact_dialog" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Enter your name</h4>
                    </div>
                    <div class="modal-body">
                        <form id="contact_form" action="<?php echo get_template_directory_uri();?>/contact_broker_ajax.php" method="POST">
                            First name: <input type="text" name="first_name"><br/>
                            Last name: <input type="text" name="last_name"><br/>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" id="submitForm" class="btn btn-default">Send</button>
                    </div>
                </div>
            </div>
        </div>

    <?php
    //print display_breadcrumbs( $options['full_breadcrumbs'] ,$options['bread_align_internal'] )
    ?>



    
			<link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet" type="text/css" media="all" />
		<link href="http://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css" media="all" />
		<link href="<?php echo get_template_directory_uri();?>/css/normalize.css" rel="stylesheet" type="text/css" media="all" />
		<link href="<?php echo get_template_directory_uri();?>/css/responsive-accordion.css" rel="stylesheet" type="text/css" media="all" />
		<link href="<?php echo get_template_directory_uri();?>/css/style.min.css" rel="stylesheet" type="text/css" media="all" />
	  

        <!-- begin content--> 
        <?php /*<div id="post" class=" agentborder <?php print $options['grid']. ' ' . $options['shadow']; ?>"> */?>
		<div id="post" class="  twelve" style="padding-top:0px;"> 
            <div class="inside_post inside_no_border" style="margin-bottom:0px;">
               <form method="post" action="<?php echo get_permalink(4690)?>" class="twelve columns alpha nomargin" style="padding:24px;background:#005375;">
			    <p style="color:#F2F2F2 !important ;font-size:14px;">Find the perfect commercial real estate
				broker when you are looking to buy or lease any type of property, including 
				Hotel, Industrial, Land, Multifamily, Office, Retail,  Specialty.</p>
			   <div class="seven columns alpha nomargin">
			   <input type="text" name="name1" placeholder="search by name or company " class="five columns alpha nomargin" style="box-shadow: none;margin-left:2px !important;margin-right:2px !important;"/>
			   <input type="text" name="city" placeholder="search by location" class="five columns alpha nomargin" style="box-shadow: none;margin-left:2px !important;margin-right:2px !important;border-shadow:none;"/>
			   <input type="hidden" name="brokers" placeholder="" class="two columns alpha nomargin" />
			   
			   
			   <input type="submit" name="broker" value="Find" class="two columns alpha nomargin" style="margin-left:2px !important;margin-right:2px !important;border-radius:0;color:#fff !important;"/>
              </div>			  
			  </form>
			   
			   <div style="padding:30px;background:#f7f7f7;height:100%;display:inline-block;width:100%;">
			   <h1 class="entry-title-agent-page"><?php the_title(); ?></h1>
			   <div class="twelve columns alpha nomargin"style="padding:10px;border: 1px solid #ddd;border-bottom:none;background:#FAFAFA;">
			    <div class="one columns alpha nomargin"></div>
				<div class="two columns alpha nomargin" style="font-weight:bold;color:#86888a !important;">Name</div>
				<div class="five columns alpha nomargin" style="font-weight:bold;color:#86888a !important;">Company</div>
				<div class="two columns alpha nomargin" style="font-weight:bold;color:#86888a !important;">Listing</div>
				<div class="two columns alpha nomargin" style="font-weight:bold;color:#86888a !important;"></div>
				<div class="one columns alpha nomargin"></div>
			   </div>

<ul class="responsive-accordion responsive-accordion-default bm-larger">			   
                <?php while (have_posts()) : the_post(); ?>
                <?php if ( esc_html (get_post_meta($post->ID, 'page_show_title', true) ) != 'no') { ?>
                    
                <?php } ?>	
                <?php the_content(); ?>
                <?php endwhile; // end of the loop.  ?>  

                <?php
				$name=$_GET['name1'];
				$city=$_GET['city'];
				$brokers=$_GET['brokers'];
				$c=", array(
					'key' => 'agent_city',
					'value' => $name,
					'compare' => 'like',
					)";
				if(isset($name) && $city==''){
	                $args = array(
                    'post_type' => 'estate_agent',
					'post_status' => 'publish',
					//'orderby' => $name,
                    'paged' => $paged,
					'posts_per_page' => 5,
					'meta_query' => array(
					'relation' => 'OR',
					array(
					'key' => 'post_title',
					'value' => $name,
					'compare' => 'LIKE',
					),
					array(
					'key' => 'agent_company',
					'value' => $name,
					'compare' => 'LIKE',
					)
					
					)
					);
				}
				elseif(isset($city) && $name==''){
					$args = array(
                    'post_type' => 'estate_agent',
					'post_status' => 'publish',
					//'orderby' => $name,
                    'paged' => $paged,
					'posts_per_page' => 5,
					'meta_query' => array(
					'relation' => 'OR',
					array(
					'key' => 'agent_city',
					'value' => $city,
					'compare' => 'LIKE',
					)		
					
					)
					);	
				}
				elseif(isset($city) && isset($name)){
					$args = array(
                    'post_type' => 'estate_agent',
					'post_status' => 'publish',
					//'orderby' => $name,
                    'paged' => $paged,
					'posts_per_page' => 5,
					'meta_query' => array(
					array(
					'key' => 'agent_city',
					'value' => $city,
					'compare' => 'LIKE',
					),
					array(
					'key' => 'post_title',
					'value' => $name,
					'compare' => 'LIKE',
					)				
					)
					);	
				}
				else {
						$args = array(
						'post_type' => 'estate_agent',
						'post_status' => 'publish',
						//'orderby' => $name,
						'paged' => $paged,
						'posts_per_page' => 5
						);
				}				               
                $agent_selection = new WP_Query($args);
				//echo "<pre/>";
				//print_r($agent_selection);die;
				//echo count($agent_selection);?>
				
				<?php 
				if($agent_selection->found_posts==0){
					
					print "Nothing found";
				} else {
                while ($agent_selection->have_posts()): $agent_selection->the_post();

                    $thumb_id = get_post_thumbnail_id($post->ID);
                    $preview = wp_get_attachment_image_src(get_post_thumbnail_id(), 'small_thumb');
                    $agent_skype    = esc_html( get_post_meta($post->ID, 'agent_skype', true) );
                    $agent_phone    = esc_html( get_post_meta($post->ID, 'agent_phone', true) );
                    $agent_mobile   = esc_html( get_post_meta($post->ID, 'agent_mobile', true) );
                    $agent_email    = esc_html( get_post_meta($post->ID, 'agent_email', true) );
                    $agent_posit    = esc_html( get_post_meta($post->ID, 'agent_position', true) );
					$agent_comp    = esc_html( get_post_meta($post->ID, 'agent_company', true) );
                    $name           = get_the_title();
                    ?>
						<?php  $args = array(
                    'post_type'         =>  'estate_property',
                    'post_status'       =>  'publish',
                    'meta_key'          =>  'prop_featured',
					'order'             =>  'DESC',
					'meta_query'        =>  array(
                                                array(
                                                    'key' => 'property_agent',
                                                    'value' => $post->ID,
                                                )
                                            )
                    );
                
                $prop_selection1 =   new WP_Query($args);
               $listings=$prop_selection1->post_count;
                ?>
				<li style="padding:0px;margin:0px;">
					<div class="responsive-accordion-head twelve alpha nomargin" style="box-shadow: 0 1px 5px -2px rgba(0, 0, 0, 0.1);">
					<div class="one columns alpha nomargin">
					<?php if($preview[0]!=''){?>
					<div class="featured_agent_image" data-agentlink="<?php echo get_permalink();?>" style="background-image: url('<?php print $preview[0]; ?>');">
                      </div>
					<?php } else {?>
					<div class="featured_agent_image" data-agentlink="<?php echo get_permalink();?>" style="background-image: url('<?php echo get_template_directory_uri();?>/images/default_profile.png');">
                      </div>
					<?php }?>
					</div>							 
                    <div class="agent_listing_details two columns alpha nomargin" style="padding-top:12px;">
						<?php
                            print '<a href="' . get_permalink() . '" style="color:#1f84dc;">' . $name. '</a>';?></div>
							<div class="agent_listing_details five columns alpha nomargin" style="color:#86888a;padding-left:8px;padding-top:12px;">
						<?php echo $agent_comp;?></div>
						<div class="agent_listing_details two columns alpha nomargin" style="color:#86888a;padding-left:15px;padding-top:12px;">
						<?php echo $listings;?>
                    </div>
					<div class="two columns alpha nomargin" style="padding-top:10px;">
					 <button type="button" class="btn btn-info" data-toggle="popover" title="<?php global $current_user;if($current_user->ID == ''){?>Please login First<?php } else {?>phone<?php }?>" 
					 data-content="<?php if($current_user->ID!=''){echo $agent_mobile;}?>"><img class="ph" src="<?php print get_template_directory_uri(); ?>/images/phone_icon.png" alt="logo" style="height:26px;" /></button>

					 <button type="button" class="btn btn-info" data-toggle="popover" title="<?php global $current_user;if($current_user->ID == ''){?>Please login First<?php } else {?>Email<?php }?>" 
					 data-content="<?php if($current_user->ID!=''){echo $agent_email;}?>">
					 <img src="<?php print get_template_directory_uri(); ?>/images/mail_icon.png" alt="logo" style="height:26px;margin-left:5px;" />
					</button>

					</div>
						
						
						
 
					<div class="agent_listing_details one columns alpha nomargin">
					
					<i class="sha fa fa-chevron-down responsive-accordion-plus fa-fw" data-toggle="collapse"></i>
					<i class="sha fa fa-chevron-up responsive-accordion-minus fa-fw"></i>
					</div>
					</div>
					<div class="responsive-accordion-panel" style="display:inline-block;margin-top:-8px;width:100%;">
											<?php  $args = array(
                    'post_type'         =>  'estate_property',
                    'post_status'       =>  'publish',
                    'meta_key'          =>  'prop_featured',
					'posts_per_page'          => 3,
					'order'             =>  'DESC',
					'meta_query'        =>  array(
                                                array(
                                                    'key' => 'property_agent',
                                                    'value' => $post->ID,
                                                )
                                            )
                    );
                
                $prop_selection =   new WP_Query($args);
               //$listings=$prop_selection->post_count;
                ?>

						<?php 
		$counter = 0;
                if ( $prop_selection->have_posts() ) {
                    
                        $rental_module_status= esc_html ( get_option('wp_estate_enable_rental_module','') );
                    print'<div class="inside_post  agentstuff bottom-estate_property"><h2 class="mylistings1">'; 
                    _e('','wpestate').
                    print'</h2>';
                    while ($prop_selection->have_posts()): $prop_selection->the_post();                     
                        if ($counter % $options['related_no'] == 0) {
                            $is_last = 'is_last';
                        } else {
                            $is_last = '';
                        }

                        if (($counter - 1) % $options['related_no']== 0) {
                            $is_first = 'is_first';
                        } else {
                            $is_first = '';
                        }
                        
                        /*if( $rental_module_status=='yes'){
                            include(locate_template('prop-listing-booking.php'));
                        }else{
                            include(locate_template('prop-listing.php'));
                        }*/
                   
                        ?>
<?php
$counter++;
$preview=array();
$preview[0]='';

$favorite_class='icon-fav-off';

if($curent_fav){
    if ( in_array ($post->ID,$curent_fav) ){
    $favorite_class='icon-fav-on';          
    } 
}
    

?>  

<div id="listing<?php print $counter;?>" class="property_listing">

        <?php
       
        if (has_post_thumbnail()):
           
        
            $preview = wp_get_attachment_image_src(get_post_thumbnail_id(), 'property_listings');
            $extra= array(
                'data-original'=>$preview[0],
                'class'	=> 'lazyload',    
            );
       
         
            $thumb_prop = get_the_post_thumbnail($post->ID, 'property_listings',$extra);
           
            $prop_stat = esc_html( get_post_meta($post->ID, 'property_status', true) );
            $featured  = intval  ( get_post_meta($post->ID, 'prop_featured', true) );
        
         
            //print '<figure data-link="' . get_permalink() . '">'. $thumb_prop;
			print '<figure>'. $thumb_prop;
            
            if($featured==1){
                //print '<div class="featured_div">'.__('Featured','wpestate').'</div>';
            }
            
            if ($prop_stat != 'normal' && $prop_stat != '') {
                $ribbon_class = str_replace(' ', '-', $prop_stat);
                if (function_exists('icl_translate') ){
                    $prop_stat     =   icl_translate('wpestate','wp_estate_property_status'.$prop_stat, $prop_stat );
                }    
                print'<a href="' . get_permalink() . '"><div class="ribbon-wrapper-default ribbon-wrapper-' . $ribbon_class . '"><div class="ribbon-inside ' . $ribbon_class . '">' . $prop_stat . '</div></div></a>';         
                
            }
            
            print '         
                <figcaption data-link="' . get_permalink() . '">
                    <span class="fig-icon"></span>               
                </figcaption>              
            </figure>
            '; 
			print '         
                <figcaption>
                    <span class="fig-icon"></span>               
                </figcaption>              
            </figure>
            ';
        endif;

        $price = intval( get_post_meta($post->ID, 'property_price', true) );
        if ($price != 0) {
           $price = number_format($price);

           if ($where_currency == 'before') {
               $price = $currency . ' ' . $price;
           } else {
               $price = $price . ' ' . $currency;
           }
        }else{
            $price='';
        }
        
        $property_address       =   esc_html ( get_post_meta($post->ID, 'property_address', true) );
        $property_city          =   get_the_term_list($post->ID, 'property_city', '', ', ', '') ;
        $price_label            =   esc_html ( get_post_meta($post->ID, 'property_label', true) );
        $property_rooms         =   floatval  ( get_post_meta($post->ID, 'property_bedrooms', true) );
        $property_bathrooms     =   floatval  ( get_post_meta($post->ID, 'property_bathrooms', true) );
        $property_size          =   number_format ( floatval  ( get_post_meta($post->ID, 'property_size', true) ) );
        $measure_sys            =   esc_html ( get_option('wp_estate_measure_sys','') ); 
       
        if($measure_sys== __('meters','wpestate') ){
            $measure_sys='m';
        }else{
             $measure_sys='ft';
        }
        ?>

        <div class="property_listing_details">
            <h3 class="listing_title"><a href="<?php echo the_permalink(); ?>"><?php the_title(); ?></a> </h3>
            
           <span class="property_price"><?php print $price.' '.$price_label; ?></span> 
           
            
           
           <?php /* <div class="article_property_type listing_units">
                <?php   if($property_rooms!=0 ) {?>
                            <span class="inforoom">  <?php print $property_rooms;?></span>
                <?php   }
                        if($property_bathrooms!=0 ){
                ?>
                        <span class="infobath">  <?php print $property_bathrooms;?></span>
                <?php   } if($property_size!=0 ){ ?>
                        <span class="infosize"> <?php print $property_size.' '.$measure_sys.'<sup>2</sup>';?></span>
                <?php } ?>
            </div>*/?>
           
           <div class="article_property_type"><?php  print $property_address.', '.$property_city; ?> </div>
            
            <div class="article_property_type">
                <?php echo get_the_term_list($post->ID, 'property_category', '', ', ', '');?> 
                <?php $action_list= get_the_term_list($post->ID, 'property_action_category', '', ', ', '');
                    if($action_list!=''){
                        print __('in','wpestate').' '.$action_list;
                    }
                ?>
            </div>
      
        </div>          

      
</div>




                     <?php endwhile; 
                     print '</div>';
                } 
                
                ?>
		
					</div>
				</li>
				  

                    <?php endwhile; ?>
					</ul>
					
             <!-- end inside post-->
            <?php kriesi_pagination($agent_selection->max_num_pages, $range = 2); ?> 
				<?php }			?>
		

        </div></div></div>
        <!-- end content-->




       <?php  //include(locate_template('customsidebar.php')); ?>

    </div><!-- #main -->    
</div><!-- #wrapper -->
<?php get_footer(); ?>
<style>
.featured_agent_image {
	width:50px;
	height:50px;
}
.agentstuff .property_listing:nth-of-type(3n), #listing_ajax_container .property_listing:nth-of-type(3n){
	float:left;
	margin-left:0px;
}
</style>