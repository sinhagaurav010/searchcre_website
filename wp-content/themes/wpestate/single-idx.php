<?php
// Template Name: Idx Page
// Wp Estate Pack
get_header();
$options=sidebar_orientation($post->ID); 
?>


<!-- Google Map Code -->
<?php 
get_template_part('libs/templates/map-template'); 
?> 
<!-- Google Map Code -->


<div id="wrapper" class="<?php print $options['fullwhite']; ?>">  
    <div class="<?php print $options['add_back']; ?>"></div>

    <?php
    print breadcrumb_container($options['full_breadcrumbs'],$options['bread_align'] )
    ?>
    <div id="main" class="row <?php print $options['sidebar_status']; ?>">
    <?php
    print display_breadcrumbs( $options['full_breadcrumbs'] ,$options['bread_align_internal'] )
    ?>

    <!-- begin content --> 
    <?php
    $classes = array(
                $options['grid'],
                $options['shadow'],
                'singlepost',
                'listingborder'
                );
    ?>
    <div id="post" <?php post_class($classes);?> > 
        <div class="inside_post  inside_no_border" >

                <?php 
                while ( have_posts() ) : the_post();
                if (esc_html( get_post_meta($post->ID, 'post_show_title', true) ) != 'no') { ?> 
                    <h1 class="entry-title" ><?php the_title(); ?></h1>
                <?php }  ?>

  
                <div class="single-content foridx">
                    <?php   the_content('Continue Reading');    ?>                   
                   
                </div>
                
               
                

	<?php endwhile; // end of the loop. ?>

            </div><!-- end inside post-->
        </div>
        <!-- end content-->



       <?php  include(locate_template('customsidebar.php')); ?>

    </div><!-- #main -->    
</div><!-- #wrapper -->
<?php get_footer(); ?>