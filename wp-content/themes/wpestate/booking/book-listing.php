<?php
global $post;
global $where_currency;
global $currency;

$link               =   get_permalink();
$booking_status     =   get_post_meta($post->ID, 'booking_status', true);
$booking_id         =   get_post_meta($post->ID, 'booking_id', true);
$booking_from_date  =   get_post_meta($post->ID, 'booking_from_date', true);
$booking_to_date    =   get_post_meta($post->ID, 'booking_to_date', true);
$booking_guests     =   get_post_meta($post->ID, 'booking_guests', true);
$preview            =   wp_get_attachment_image_src(get_post_thumbnail_id($booking_id), 'property_sidebar');
$author             =   get_the_author();

$invoice_no         =   get_post_meta($post->ID, 'booking_invoice_no', true);
$booking_pay        =   get_post_meta($post->ID, 'booking_pay_ammount', true);
$booking_company    =   get_post_meta($post->ID, 'booking_company', true);
    

$no_of_days         = ( strtotime($booking_to_date)-strtotime($booking_from_date) ) / (60*60*24);
$property_price     =   get_post_meta($booking_id, 'property_price', true);

$price_per_option   =   intval(get_post_meta($booking_id, 'price_per', true));
if ($price_per_option!=0){
    $property_price     =   round ( $property_price/$price_per_option,2);
}
$price_per_booking  =   $no_of_days *$property_price;
$event_description  =   get_the_content();   




if($invoice_no== 0){
    $invoice_no='-';
}else{
    $price_per_booking=get_post_meta($invoice_no, 'item_price', true);
}

$price_per_booking = number_format($price_per_booking,2,'.',',');

if ($where_currency == 'before') {
    $price_per_booking = $currency . ' ' . '<span id="inv_new_price">'.$price_per_booking . '</span> ';
} else {
    $price_per_booking = '<span id="inv_new_price">'.$price_per_booking . '</span> ' . $currency;
}

?>


<div class="dasboard-prop-listing">
   <div class="blog_listing_image">
        <a href="<?php print get_permalink($booking_id);?>">
           <img  src="<?php  print $preview[0]; ?>"  alt="slider-thumb" />
        </a>
   </div>
    

    <div class="prop-info">
        <h3 class="listing_title">
           <?php the_title();  
                print ' <strong>'. __('for','wpestate').'</strong> <a href="'.get_permalink($booking_id).'">'.get_the_title($booking_id).'</a>'; ?>      
        </h3>
        
        
        
        <div class="user_dashboard_listed">
            <strong>  <?php _e('Request by ','wpestate');?></strong><?php print $author ?>
        </div>
        
        <div class="user_dashboard_listed">
            <strong><?php _e('Period: ','wpestate');?>   </strong>  <?php print $booking_from_date.' <strong>'.__('to','wpestate').'</strong> '.$booking_to_date; ?>
        </div>
        
        <div class="user_dashboard_listed">
            <strong><?php _e('Invoice No: ','wpestate');?></strong> <span class="invoice_list_id"><?php print $invoice_no;?></span>   
        </div>
        
        <div class="user_dashboard_listed">
            <strong><?php _e('Pay Amount: ','wpestate');?> </strong> <?php print $price_per_booking; ?>  
        </div>
        
        <div class="user_dashboard_listed">
            <strong><?php _e('Guests: ','wpestate');?> </strong> <?php print $booking_guests; ?>  
        </div>  
        
        <?php 
        if($event_description!=''){
            print ' <div class="user_dashboard_listed event_desc"><strong>'.__('Comments: ','wpestate').'</strong>'.$event_description.'</div>';
        }
        ?>
          
        
    </div>

    
    <div class="info-container_booking">
        <?php //print $booking_status;
        if ($booking_status=='confirmed'){
            print '<span class="tag-published">'.__('Confirmed','wpestate').'</span>';
            print '<span class="tag-published confirmed_booking" data-invoice-confirmed="'.$invoice_no.'" data-booking-confirmed="'.$post->ID.'">'.__('View Details','wpestate').'</span>';
      
        }else if( $booking_status=='waiting'){
            print '<span class="waiting_payment" data-bookid="'.$post->ID.'">'.__('Invoice Issued - waiting for payment','wpestate').'</span>';             
            print '<span class="delete_invoice" data-invoiceid="'.$invoice_no.'" data-bookid="'.$post->ID.'">'.__('Delete Invoice','wpestate').'</span>';
            print '<span class="delete_booking" data-bookid="'.$post->ID.'">'.__('Reject Booking Request','wpestate').'</span>';    
        }else{
            print '<span class="generate_invoice" data-bookid="'.$post->ID.'">'.__('Issue invoice','wpestate').'</span>';  
            print '<span class="delete_booking" data-bookid="'.$post->ID.'">'.__('Reject Booking Request','wpestate').'</span>';    
        } 
       
        ?>
        
    </div>
         
 </div>