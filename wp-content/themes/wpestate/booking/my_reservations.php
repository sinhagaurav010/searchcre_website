<?php
// Template Name: Reservations List
// Wp Estate Pack



if ( !is_user_logged_in() ) {   
     wp_redirect(  home_url() );exit;
} 

global $current_user;
global $where_currency;
global $currency;
get_currentuserinfo();
$userID                         =   $current_user->ID;
$user_login                     =   $current_user->user_login;
$user_group                     =   get_the_author_meta( 'user_group' , $userID );
$user_pack                      =   get_the_author_meta( 'package_id' , $userID );
$user_registered                =   get_the_author_meta( 'user_registered' , $userID );
$user_package_activation        =   get_the_author_meta( 'package_activation' , $userID );   
$paid_submission_status         =   esc_html ( get_option('wp_estate_paid_submission','') );
$price_submission               =   floatval( get_option('wp_estate_price_submission','') );
$submission_curency_status      =   esc_html( get_option('wp_estate_submission_curency','') );
$where_currency                 =   esc_html( get_option('wp_estate_where_currency_symbol', '') );
$currency                       =   esc_html( get_option('wp_estate_submission_curency', '') );
    
get_header();
$options=sidebar_orientation($post->ID);
get_template_part('libs/templates/map-template');    
?> 
<!-- Google Map Code -->



<div id="wrapper" class="fullwhite">  
    <div class="<?php print $options['add_back']; ?>"></div>

     <?php
    print breadcrumb_container('yes',$options['bread_align'] )
    ?>
    <div id="main" class="row">
    <?php
    print display_breadcrumbs( 'yes' ,$options['bread_align_internal'] )
    ?>




        <!-- begin content--> 
        <div id="post" class=" noborder twelve columns alpha noshadow"> 
            <div class="inside_post inside_no_border submit_area">
               <?php if (esc_html( get_post_meta($post->ID, 'page_show_title', true) ) != 'no') { ?>
                    <h1 class="entry-title-agent-page"><?php the_title(); ?></h1>
               <?php } ?>
                  
               <?php get_template_part('libs/templates/user_menu');   ?>
                    
                    
                <div class="booking-keeper">
                <?php
                    wp_reset_query();
                    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                    

                    $args = array(
                        'post_type'         => 'wpestate_booking',
                        'post_status'       => 'publish',
                        'paged'             => $paged,
                        'posts_per_page'    => 30,
                        'order'             => 'DESC',
                        'author'            =>  $userID    
                     );
                    
                    $book_selection = new WP_Query($args);
                    
                    if ( $book_selection->have_posts() ){
                        while ($book_selection->have_posts()): $book_selection->the_post(); 
                            include(locate_template('booking/book-listing-user.php'));
                        endwhile;
                    }else{
                        print '<h4>'.__('You don\'t have any reservations made!','wpestate').'</h4>';
                    } 
                    wp_reset_query(); 
                    ?>
                
                </div>
                    
                    
              
            </div> <!-- end inside post-->
            <?php   kriesi_pagination($book_selection->max_num_pages, $range =2); ?>
        </div>
        <!-- end content-->

       <?php //   include(locate_template('customsidebar.php')); ?>

    </div><!-- #main -->    
</div><!-- #wrapper -->
<?php get_footer(); ?>
