<?php
global $post;
$message_from_user    =   get_post_meta($post->ID, 'message_from_user', true);
$message_status       =   get_post_meta($post->ID, 'message_status', true);
$message_title        =   get_the_title($post->ID);
$message_content      =   get_the_content();


?>


<div class="message_listing" data-messid="<?php print $post->ID; ?> ">
    
    <div class="message_header">
        <?php
        if($message_status=='unread'){
            print '<span class="mess_unread">'.__('unread','wpestate').'</span>';        
        }else{
            print '<span class="mess_read">'.__('read','wpestate').'</span>';
        }
        ?>

        <span class="mess_from">    <strong><?php _e('From','wpestate');?>: </strong>   <?php print $message_from_user;?></span>
        <span class="mess_subject"> <strong><?php _e('Subject','wpestate');?>: </strong><?php print $message_title;?></span>
        <span class="mess_date">    <?php echo get_the_date(); ?>   </span>
        
        <span class="mess_delete">  <?php _e('delete','wpestate');?></span>
        <span class="mess_reply">   <?php _e('reply','wpestate');?> </span>
        <span class="mess_read_mess">    <?php _e('read message','wpestate');?>  </span>
    </div>   
    
    <div class="mess_content">
        <h4><?php print $message_title;?></h4></span>
        <span class="message_content"><?php print $message_content;?></span>
       
        
        <?php // show_mess_reply($post->ID); ?>
    </div>
    
     <div class="mess_reply_form">
        <form method="post" action="">
            <h4><?php _e('Reply','wpestate');?></h4>
            <input type="text" id="subject_reply" value="Re: <?php echo $message_title; ?>">
            <textarea name="message_reply_content" id="message_reply_content"></textarea></br>
            <span id="mess_send_reply" class="mess_send_reply"><?php _e('Send Reply','wpestate');?></span>
        </form>  
     </div>    
    
    
</div>