<?php
// this is the slider for the blog post
// embed_video_id embed_video_type
$video_id='';
$video_thumb='';
$video_alone = 0;
$full_img='';
 if (esc_html( get_post_meta($post->ID, 'group_pictures', true) ) == 'yes') {
        $arguments = array(
            'numberposts' => -1,
            'post_type' => 'attachment',
            'post_parent' => $post->ID,
            'post_status' => null,
            'exclude' => get_post_thumbnail_id(),
            'orderby' => 'menu_order',
            'order' => 'ASC'
        );
        $post_attachments = get_posts($arguments);

        if ($post_attachments || has_post_thumbnail() || get_post_meta($post->ID, 'embed_video_id', true)) {  ?>   
            <div class="custom_slider">
                <ul id="main-carusel" class="elastislide-list">
                    <?php
                    // get video thumb 
                    if (get_post_meta($post->ID, 'embed_video_id', true)) {
                        $video_id = esc_html( get_post_meta($post->ID, 'embed_video_id', true) );
                        $video_type = esc_html( get_post_meta($post->ID, 'embed_video_type', true) );
                        if($video_type=='vimeo'){
                             $hash2 = ( wp_remote_get("http://vimeo.com/api/v2/video/$video_id.php") );
                             $pre_tumb=(unserialize ( $hash2['body']) );
                             $video_thumb=$pre_tumb[0]['thumbnail_medium'];                                        
                        }else{
                            $video_thumb = 'http://img.youtube.com/vi/' . $video_id . '/0.jpg';
                        }
                    } // end if post_video_id
                    
                    if (has_post_thumbnail() && !$video_id) {
                        $thumb_id = get_post_thumbnail_id($post->ID);
                        $preview = wp_get_attachment_image_src(get_post_thumbnail_id(), 'blog_thumb');
                        $full_img = wp_get_attachment_image_src(get_post_thumbnail_id(), 'blog-full');
                        ?> 
                        <li data-preview="<?php print $full_img[0]; ?>">
                            <img width="133" height="93" src="<?php print $preview[0]; ?>"  alt="slider" />

                        </li>
                    <?php
                     $video_alone = 0;
                    }
                    // add image thumb
                    foreach ($post_attachments as $attachment) {
                        $preview = wp_get_attachment_image_src($attachment->ID, 'blog_thumb');
                        $full_img = wp_get_attachment_image_src($attachment->ID, 'blog-full');
                        ?>
                        <li data-preview="<?php print $full_img[0]; ?>">
                            <img width="133" height="93" src="<?php print $preview[0]; ?>" alt="slider"/>

                        </li>
                        <?php
                        $video_alone = 0;
                    }
                    // add video thumb
                    if ($video_thumb != '') { ?>
                        <li class="video_thumb_force" data-video_data="<?php print $video_type; ?>" data-video_id="<?php print $video_id; ?>"  style="max-height:93px;">
                            <?php
                            print '<img src="' . $video_thumb . '" alt="video thumb" ';
                            if ($video_alone == 1)
                                print ' width="143" ';
                            print'/>';
                            ?>
                            <span class="video_play"></span>
                        </li>
                    <?php } ?>  

                </ul>

                <div class="image-preview" id="img-preview">
                    <a href="" id="preview_link"  data-pretty="prettyPhoto"><img id="preview" style="display: none;" src="<?php print $full_img[0]; ?>" alt="preview"/></a>
                    <div class="image_loader initial_loader"></div>
                    <span id="control_prev"></span>
                    <span id="control_next"></span>
                </div>
                <div class="custom_slider_shadow"></div>
                <div id="closer"></div>

            </div>              
            <?php
            } // end if post_attachments
            ?>               
<?php
 } //end grup pictures
?>
