<?php

///////////////////////////////////////////////////////////////////////////////////////////
/////// register shortcodes
///////////////////////////////////////////////////////////////////////////////////////////

add_action('init', 'register_shortcodes');
add_action('init', 'tiny_short_codes_register');

// allow shortcodes in widget
add_filter('widget_text', 'do_shortcode');

///////////////////////////////////////////////////////////////////////////////////////////
// register tiny plugins functions
///////////////////////////////////////////////////////////////////////////////////////////

function tiny_short_codes_register() {
    if (!current_user_can('edit_posts') && !current_user_can('edit_pages')) {
        return;
    }
    if (get_user_option('rich_editing') == 'true') {
        add_filter('mce_external_plugins', 'add_plugin');
        add_filter('mce_buttons_3', 'register_button');
        add_filter('mce_buttons_4', 'register_button_2');
    }
}

/////////////////////////////////////////////////////////////////////////////////////////
/////// push the code into Tiny buttons array
///////////////////////////////////////////////////////////////////////////////////////////
function register_button($buttons) {
    array_push($buttons, "|", "slider");
    array_push($buttons, "|", "container_content_main");
    array_push($buttons, "|", "one_half_column");
    array_push($buttons, "|", "one_third_column");
    array_push($buttons, "|", "two_third_column");
    array_push($buttons, "|", "one_fourth_column");
    array_push($buttons, "|", "three_fourth_column");
    array_push($buttons, "|", "checklist");
    array_push($buttons, "|", "tabs");
    array_push($buttons, "|", "toggle");
    array_push($buttons, "|", "accordion");
    array_push($buttons, "|", "tagline");
    array_push($buttons, "|", "container_content_regular");
    array_push($buttons, "|", "regular_table");
    array_push($buttons, "|", "wpbutton");

    return $buttons;
}

function register_button_2($buttons) {
    array_push($buttons, "|", "highlight");
    array_push($buttons, "|", "dropcap");
    array_push($buttons, "|", "testimonials");
    array_push($buttons, "|", "recent_items");
    array_push($buttons, "|", "latest_products");
    array_push($buttons, "|", "vimeo");
    array_push($buttons, "|", "youtube");
 //   array_push($buttons, "|", "google_map");
   // array_push($buttons, "|", "spacer");
    array_push($buttons, "|", "featured_agent");
    array_push($buttons, "|", "featured_article");
    array_push($buttons, "|", "featured_property");
    array_push($buttons, "|", "list_items_by_id");
    array_push($buttons, "|", "login_form");
    array_push($buttons, "|", "register_form");
    array_push($buttons, "|", "advanced_search");
    array_push($buttons, "|", "space_area");
    array_push($buttons, "|", "font_awesome");
    return $buttons;
}
///////////////////////////////////////////////////////////////////////////////////////////
/////// poins to the right js 
///////////////////////////////////////////////////////////////////////////////////////////

function add_plugin($plugin_array) {
    $plugin_array['youtube']                    = get_template_directory_uri() . '/js/shortcodes.js';
    $plugin_array['vimeo']                      = get_template_directory_uri() . '/js/shortcodes.js';
    $plugin_array['wpbutton']                   = get_template_directory_uri() . '/js/shortcodes.js';
    $plugin_array['dropcap']                    = get_template_directory_uri() . '/js/shortcodes.js';
    $plugin_array['highlight']                  = get_template_directory_uri() . '/js/shortcodes.js';
    $plugin_array['checklist']                  = get_template_directory_uri() . '/js/shortcodes.js';
    $plugin_array['tabs']                       = get_template_directory_uri() . '/js/shortcodes.js';
    $plugin_array['testimonials']               = get_template_directory_uri() . '/js/shortcodes.js';
    $plugin_array['tagline']                    = get_template_directory_uri() . '/js/shortcodes.js';
    $plugin_array['recent_items']               = get_template_directory_uri() . '/js/shortcodes.js';
    $plugin_array['container_content_main']     = get_template_directory_uri() . '/js/shortcodes.js';
    $plugin_array['container_content_regular']  = get_template_directory_uri() . '/js/shortcodes.js';
    $plugin_array['one_half_column']            = get_template_directory_uri() . '/js/shortcodes.js';
    $plugin_array['one_third_column']           = get_template_directory_uri() . '/js/shortcodes.js';
    $plugin_array['two_third_column']           = get_template_directory_uri() . '/js/shortcodes.js';
    $plugin_array['one_fourth_column']          = get_template_directory_uri() . '/js/shortcodes.js';
    $plugin_array['three_fourth_column']        = get_template_directory_uri() . '/js/shortcodes.js';
    $plugin_array['slider']                     = get_template_directory_uri() . '/js/shortcodes.js';
    $plugin_array['accordion']                  = get_template_directory_uri() . '/js/shortcodes.js';
    $plugin_array['toggle']                     = get_template_directory_uri() . '/js/shortcodes.js';
    $plugin_array['breadcrumbs']                = get_template_directory_uri() . '/js/shortcodes.js';
    $plugin_array['regular_table']              = get_template_directory_uri() . '/js/shortcodes.js';
 //   $plugin_array['google_map']                 = get_template_directory_uri() . '/js/shortcodes.js';
    $plugin_array['latest_products']            = get_template_directory_uri() . '/js/shortcodes.js';
 //   $plugin_array['spacer']                     = get_template_directory_uri() . '/js/shortcodes.js';
    $plugin_array['featured_agent']             = get_template_directory_uri() . '/js/shortcodes.js';
    $plugin_array['featured_article']           = get_template_directory_uri() . '/js/shortcodes.js';
    $plugin_array['featured_property']          = get_template_directory_uri() . '/js/shortcodes.js';
    $plugin_array['login_form']                 = get_template_directory_uri() . '/js/shortcodes.js';
    $plugin_array['register_form']              = get_template_directory_uri() . '/js/shortcodes.js';
    $plugin_array['list_items_by_id']           = get_template_directory_uri() . '/js/shortcodes.js';
    $plugin_array['advanced_search']            = get_template_directory_uri() . '/js/shortcodes.js';
    $plugin_array['space_area']                 = get_template_directory_uri() . '/js/shortcodes.js';
    $plugin_array['font_awesome']               = get_template_directory_uri() . '/js/shortcodes.js';
   


    return $plugin_array;
}

///////////////////////////////////////////////////////////////////////////////////////////
/////// register shortcodes
///////////////////////////////////////////////////////////////////////////////////////////


function register_shortcodes() {
    add_shortcode('recent-posts', 'recent_posts_function');
    add_shortcode('youtube', 'youtube_video');
    add_shortcode('vimeo', 'vimdeo_video');
    add_shortcode('button', 'wpestate_button');
    add_shortcode('dropcap', 'dropcap_function');
    add_shortcode('highlight', 'highlight_functions');
    add_shortcode('check-list', 'checklist_function');
    add_shortcode('tabs', 'tabs_container');
    add_shortcode('tab', 'tab_single');
    add_shortcode('container_content', 'main_container');
    add_shortcode('container_box', 'regular_container');
    add_shortcode('tagline_container', 'tagline_container');
    add_shortcode('testimonial', 'testimonial');
    add_shortcode('recent_items', 'recent_posts_pictures');
    add_shortcode('container_half', 'content_half');
    add_shortcode('container_one_third', 'container_one_third');
    add_shortcode('container_two_third', 'container_two_third');
    add_shortcode('content_one_fourth', 'content_one_fourth');
    add_shortcode('content_three_fourth', 'content_three_fourth');
    add_shortcode('slider', 'slider_shortcode');
    add_shortcode('slide', 'slide_part_shortcode');
    add_shortcode('accordion', 'accordeon_shortcode');
    add_shortcode('toggle', 'toogle_shortcode');
    add_shortcode('breadcrumbs', 'breadcrumbs_shortcode');
    add_shortcode('table', 'regular_table_shortcode');
   // add_shortcode('google_map', 'google_map_shortcode'); not available anymore
    add_shortcode('latest_products', 'latest_products_shortcode');
 //   add_shortcode('spacer', 'spacer_shortcode');
    add_shortcode('featured_agent', 'featured_agent');
    add_shortcode('featured_article', 'featured_article');
    add_shortcode('featured_property', 'featured_property');
    add_shortcode('login_form', 'login_form_function');
    add_shortcode('register_form', 'register_form_function');
    add_shortcode('list_items_by_id', 'list_items_by_id_function');
    add_shortcode('advanced_search', 'advanced_search_function');
    add_shortcode('space_area', 'spacer_shortcode_function');
    add_shortcode('spacer', 'spacer_shortcode_function');
    add_shortcode('font_awesome', 'wpestate_font_awesome_function');
}

?>