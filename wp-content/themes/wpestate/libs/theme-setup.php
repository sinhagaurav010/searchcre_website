<?php

///////////////////////////////////////////////////////////////////////////////////////////
/////// Theme Setup
///////////////////////////////////////////////////////////////////////////////////////////
add_action('after_setup_theme', 'wp_estate_init');

function wp_estate_init() {
    set_post_thumbnail_size(940, 198, true);
    add_editor_style();
    add_theme_support('post-thumbnails');
    add_theme_support('automatic-feed-links');
}



add_action('after_switch_theme', 'wp_estate_setup');
function wp_estate_setup() {    
      if (isset($_GET['activated']) && is_admin()){
        ////////////////////  insert sales and rental categories 
        $actions = array('Rentals', 'Sales');

        foreach ($actions as $key) {
            $my_cat = array(
                'description' => $key,
                'slug' => $key
            );
           if(!term_exists($key, 'property_action_category') ){
               wp_insert_term($key, 'property_action_category', $my_cat);
           }
           
        }


        ////////////////////  insert listings type categories 
        $actions = array('Apartments', 'Houses', 'Land', 'Industrial', 'Offices', 'Retail');

        foreach ($actions as $key) {
            $my_cat = array(
                'description' => $key,
                'slug' => str_replace(' ', '-', $key)
            );
            if(!term_exists($key, 'property_category') ){
                wp_insert_term($key, 'property_category', $my_cat);
            }
        }


    /* */
        ////////////////////  insert comapre and advanced search page
        $page_check = get_page_by_title('Compare Pages');
        if (!isset($page_check->ID)) {
            $my_post = array(
                'post_title' => 'Compare Pages',
                'post_type' => 'page',
                'post_status' => 'publish',
            );
            $new_id = wp_insert_post($my_post);
            update_post_meta($new_id, '_wp_page_template', 'compare-prop.php');
        }
        ////////////////////  insert comapre and advanced search page 
        $page_check = get_page_by_title('Advanced Search');
        if (!isset($page_check->ID)) {
            $my_post = array(
                'post_title' => 'Advanced Search',
                'post_type' => 'page',
                'post_status' => 'publish',
            );
            $new_id = wp_insert_post($my_post);
            update_post_meta($new_id, '_wp_page_template', 'advanced-search-results.php');
        }
    }
    
    
    add_option('wp_estate_general_latitude', '40.781711');
    add_option('wp_estate_general_longitude', '-73.955927');
    add_option('wp_estate_currency_symbol', '$');
    add_option('wp_estate_ where_currency_symbol', 'before');
    add_option('wp_estate_ where_currency_symbol', 'before');
    add_option('wp_estate_prop_no', '12');
    add_option('wp_estate_default_map_zoom', '15');
    add_option('wp_estate_blog_sidebar', 'right');
    add_option('wp_estate_blog_sidebar_name', 'primary-widget-area');
    add_option('wp_estate_measure_sys', __('feet','wpestate') );
    add_option('wp_estate_cache', 'no');
    add_option('wp_estate_geolocation', 'no');
    add_option('wp_estate_color_scheme', 'no');
    add_option('wp_estate_background_color', 'f2f2f2');
    add_option('wp_estate_content_back_color', 'ffffff');
    add_option('wp_estate_header_color', 'ffffff');
    add_option('wp_estate_breadcrumbs_back_color', 'f5f5f5');
    add_option('wp_estate_breadcrumbs_font_color', 'a3a3a3');
    add_option('wp_estate_font_color', 'a0a5a8');
    add_option('wp_estate_link_color', '#a171b');
    add_option('wp_estate_headings_color', '1a171b');
    add_option('wp_estate_comments_font_color', '717374');
    add_option('wp_estate_coment_back_color', 'f5f5f5');
    add_option('wp_estate_footer_back_color', '09723f');
    add_option('wp_estate_footer_font_color', 'ffffff');
    add_option('wp_estate_footer_copy_color', '1a171b');
    add_option('wp_estate_sidebar_widget_color', 'ffffff');
    add_option('wp_estate_menu_font_color', '1a171b');
    add_option('wp_estate_menu_hover_back_color', '22be73');
    add_option('wp_estate_menu_hover_font_color', 'ffffff');
    add_option('wp_estate_agent_color', '67cfd8');
    add_option('wp_estate_listings_color', '22be73');
    add_option('wp_estate_blog_color', 'a51e6c');
    add_option('wp_estate_dotted_line', 'd0d0d0');
    add_option('wp_estate_sidebar2_font_color', '888C8E');
    add_option('wp_estate_sidebar_heading_boxed_color', '1a171b');
    add_option('wp_estate_sidebar_heading_color', '1a171b');
    add_option('wp_estate_footer_top_band', '297A46');
    add_option('wp_estate_top_bar_back', 'f5f5f5');
    add_option('wp_estate_top_bar_font', '1a171b');
    add_option('wp_estate_cron_run', time());
 
    
    $default_feature_list='attic, gas heat, ocean view, wine cellar, basketball court, gym,pound, fireplace, lake view, pool, back yard, front yard, fenced yard, sprinklers, washer and dryer, deck, balcony, laundry, concierge, doorman, private space, storage, recreation, roof deck';
    add_option('wp_estate_feature_list', $default_feature_list);
    add_option('wp_estate_property_description_text', 'Property Description');
    add_option('wp_estate_property_details_text',  'Property Details ');
    add_option('wp_estate_property_features_text', 'Property Features');
    
    $default_status_list='open house, sold';
    add_option('wp_estate_status_list', $default_status_list);
    
    
    add_option('wp_estate_floating_property_similar_text','Similar Listings');
    add_option('wp_estate_floating_property_inquires_text','Inquires');
    add_option('wp_estate_floating_property_features_text','Features');
    add_option('wp_estate_floating_property_details_text ','Details');
    add_option('wp_estate_floating_property_description_text','Description');
    add_option('wp_estate_home_small_map','no');    
    add_option('wp_estate_facebook_login','no');
    add_option('wp_estate_google_login','no');
    add_option('wp_estate_yahoo_login','no');
    add_option('wp_estate_header_type',1);
    
    $custom_fields=array(
                    array('property year','Year Built','date',1),
                    array('property garage','Garages','short text',2),
                    array('property garage size','Garage Size','short text',3),
                    array('property date','Available from','short text',4),
                    array('property basement','Basement','short text',5),
                    array('property external construction','external construction','short text',6),
                    array('property roofing','Roofing','short text',7),
                   
    );
    
    add_option( 'wp_estate_custom_fields', $custom_fields);  
    add_option('wp_estate_enable_rental_module','no');
    add_option('wp_estate_book_down', 10);
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    $args = array(    'post_type'         => 'estate_property',
                      'posts_per_page' => -1,
                      'nopaging' => true,
                    );
    
    $prop_selection = new WP_Query($args);
           
    global $post;
    while ($prop_selection->have_posts()): $prop_selection->the_post(); 
            add_post_meta($post->ID, 'prop_featured', 0, true);
    endwhile;
    
    
    
    

}
?>
