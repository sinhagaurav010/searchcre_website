<!-- Google Map -->
<?php
$icons          =   array();
$taxonomy       =   'property_action_category';
$tax_terms      =   get_terms($taxonomy);
$taxonomy_cat   =   'property_category';
$categories     =   get_terms($taxonomy_cat);


// add only actions
foreach ($tax_terms as $tax_term) {
    $icon_name          =   limit64( 'wp_estate_icon'.$tax_term->slug);
    $limit50            =   limit50( $tax_term->slug);
    $icons[$limit50]    =   esc_html( get_option($icon_name) ); 
}

// add only categories
foreach ($categories as $categ) {
    $icon_name          =   limit64( 'wp_estate_icon'.$categ->slug);
    $limit50            =   limit50( $categ->slug);
    $icons[$limit50]    =   esc_html( get_option($icon_name) ); 
}

?>

<div class="gmap_wrapper  <?php
    if (!is_front_page()) {
        print 'gmap_not_home" style="height:295px;"';
    }else{
        print'"';
    }
    ?>>
    <div class="gmap-next <?php if (is_front_page()) print 'is-front'; ?>" id="gmap-next" ></div>
    <div class="gmap-prev <?php if (is_front_page()) print 'is-front'; ?>" id="gmap-prev" ></div>
    
    <?php 
    $geo_status     =   esc_html ( get_option('wp_estate_geolocation','') );
    $custom_image   =   '';
    $rev_slider     =   '';
    if( isset($post->ID) ){
        $custom_image   =   esc_html( esc_html(get_post_meta($post->ID, 'page_custom_image', true)) );  
        $rev_slider     =   esc_html( esc_html(get_post_meta($post->ID, 'rev_slider', true)) );
    }

    if($geo_status=='yes' && $custom_image=='' && $rev_slider==''){ 
      print'  <div id="mobile-geolocation-button"></div>';
    } 
    ?>
            
  


    <div id="googleMap" <?php
         $home_small_map_status= esc_html ( get_option('wp_estate_home_small_map','') );
        if (!is_front_page() || ( is_front_page() && $home_small_map_status=='yes' ) ) {
            print 'style="height:295px;"';
        }
        ?> >   
    </div>    
    
  
   <div class="tooltip"> <?php _e('click to enable zoom','wpestate');?></div>
   <div id="gmap-loading"><?php _e('Loading Maps','wpestate');?>
       <span class="gmap-animation">
            <img src="<?php print get_template_directory_uri(); ?>/images/ajax-loader-gmap.gif" alt="logo"/>
       </span>
   </div>
   
   
   <?php
   ////////////////////////// enable /disable map filters
   $show_filter_map_status  =  esc_html ( get_option('wp_estate_show_filter_map','') );
   $home_small_map_status   =  esc_html ( get_option('wp_estate_home_small_map','') );
   if($show_filter_map_status!='no'){
   ?>
   
   <div class="gmap-menu-wrapper" >   
        <div class="gmap-menu" id="gmap-menu" <?php if (!is_front_page() || (is_front_page() && $home_small_map_status=='yes') ) print 'style="display:none;"'; ?> >
            <div id="closefilters"></div>
            <div class="action_filter" >
                <?php
               
                foreach ($tax_terms as $tax_term) {
                        $limit50            =   limit50( $tax_term->slug);
                        print '<div class="checker"><input type="checkbox" checked="checked"  name="filter_action[]" id="'.$limit50.'" class="'.$tax_term->slug.'"  value="'.$tax_term->name.'"/><label for="'.$limit50.'"><span></span>';
                        if( $icons[$limit50]!='' ){
                             print '<img src="'.$icons[$limit50].'" alt="'.$tax_term->name.'">' . $tax_term->name . '</label></div>';
                        }else{
                             print '<img src="'.get_template_directory_uri().'/css/css-images/'.$tax_term->slug.'icon.png" alt="'.$tax_term->name.'">' . $tax_term->name . '</label></div>';
                        }
                }
                ?>
            </div> 

            <div class="type-filters">
                <?php
                foreach ($categories as $categ) {
                     $limit50            =   limit50( $categ->slug);
                     print '<div class="checker"><input type="checkbox" checked="checked"  name="filter_type[]" id="'. $limit50 .'"  class="' .  $categ->slug  . '" value="' . $categ->name . '"/><label for="' .  $limit50 . '"><span></span>';
                     if( $icons[ $limit50 ]!='' ){
                         print' <img src="'.$icons[ $limit50 ].'" alt="'.$categ->name.'">' . $categ->name . '</label></div>';
                     }else{
                         print' <img src="'.get_template_directory_uri().'/css/css-images/'.$categ->slug.'icon.png" alt="'.$categ->name.'">' . $categ->name . '</label></div>';
                     }                      
                }
                ?>  
            </div> 
        </div>
    </div>
   <?php
   }
   ?>
</div>    

<!-- END Google Map -->   