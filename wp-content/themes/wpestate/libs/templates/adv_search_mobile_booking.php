<!-- Mobile Search -->
<?php
$pages = get_pages(array(
 'meta_key' => '_wp_page_template',
 'meta_value' => 'advanced-search-results-booking.php'
     ));

if( $pages ){
    $adv_submit_booking = get_permalink( $pages[0]->ID);
}else{
     $adv_submit_booking='';
}

?>



<div id="advanced_search_map_button_mobile" <?php if (!is_front_page()) print ' style="display:none;" ';?> >
     <?php _e('Advanced Search','wpestate'); ?>
</div>    


<div class="adv-search-mobile" id="adv-search-mobile">
<div id="closeadvancedmobile"></div>    
 <form role="search" method="post"  id="advanced_booking_form_mobile" action="<?php print $adv_submit_booking; ?>" >
       
      
                    <div class="adv_search_internal advz1"> 
                        <input type="text" id="booking_location_mobile" name="booking_location" placeholder="<?php _e('Where do you want to go?','wpestate');?>"      class="advanced_select">
                    </div>

                    <div class="adv_search_internal advz2"> 
                        <input type="text" id="check_in_mobile"  name="check_in"  placeholder="<?php _e('Check In','wpestate');?>"  class="advanced_select">
                    </div>

                    <div class="adv_search_internal advz3 advanced_city_div_mobile">
                        <input type="text" id="check_out_mobile" name="check_out"  class="advanced_select" placeholder="<?php _e('Check Out','wpestate');?>"/>
                    </div>

                    <div class="adv_search_internal advz4 advanced_area_div_mobile">
                        <select id="booking_guest_mobile"  name="booking_guest"  class="cd-select" >
                            <option value="1"><?php echo '1 '.__('Guest','wpestate'); ?></option>
                            <?php
                            for ($i = 2; $i <= 11; $i++) {
                                print ' <option value="'.$i.'">'.$i.' '.__('Guests','wpestate').'</option>';
                            }

                            ?>
                        </select> 
                   </div>

                   
      

     <div class="btn vernil small mobilesubmit" id="advanced_submit_mobile" > <?php _e('Search','wpestate');?></div>

    </form>   

</div>    
<!-- End Mobile Search -->