<?php
$custom_image   =   '';
$rev_slider     =   '';
if( isset($post->ID) ){
    $custom_image   =   esc_html( esc_html(get_post_meta($post->ID, 'page_custom_image', true)) );  
    $rev_slider     =   esc_html( esc_html(get_post_meta($post->ID, 'rev_slider', true)) );
}

$hide_class='';
if ((!is_front_page() && $custom_image=='' && $rev_slider=='' ) || (is_front_page() && $home_small_map_status=='yes' ) ) {
    $hide_class= ' geohide advhome ';       
}


$show_over_search   =   get_option('wp_estate_show_adv_search','');
$dropdown_id        =   '';

if(!is_front_page() || (is_front_page() && $home_small_map_status=='yes' ) ){
    $dropdown_id='_internal'; 
}

if ($show_over_search=='yes' && ($custom_image!='' || $rev_slider!='') ){
    $dropdown_id='';
}
?>


<div id="adv-search-header-3" class="<?php echo $hide_class;?>" ><?php _e('Advanced Search','wpestate');?></div>
<div class="adv-search-3 <?php echo $hide_class;?>" id="adv-search-3" >
    <form role="search" method="post"   action="<?php print $adv_submit; ?>" >
        
        <?php
        $custom_advanced_search= get_option('wp_estate_custom_advanced_search','');
   
        if ( $custom_advanced_search == 'yes'){
            foreach($adv_search_what as $key=>$search_field){
                show_search_field($search_field,2,$dropdown_id,$actions_select,$categ_select,$select_city,$select_area,$adv_search_label,$key,$adv_search_how);
            }
        }else{
        ?>
         <div class="advanced_action_div adv1"> 
            <select id="adv_actions_2<?php echo $dropdown_id; ?>"  name="filter_search_action[]"  class="cd-select" >
                <option value="all"><?php _e('All Listings','wpestate'); ?></option>
                <?php print $actions_select; ?>
            </select>    
        </div>

        <div class="advanced_categ_div adv2"> 
            <select id="adv_categ_2<?php echo $dropdown_id; ?>"  name="filter_search_type[]" class="cd-select" >
                <option value="all"><?php _e('All Types','wpestate'); ?></option>
                <?php print $categ_select; ?>
            </select>  
        </div>

        <div class="advanced_city_div adv3">
            <select id="advanced_city_2<?php echo $dropdown_id; ?>" name="advanced_city" class="cd-select" >
                 <option value="all"><?php _e('All Cities','wpestate'); ?></option>
                <?php echo $select_city ;?>
            </select>
        </div>

        <div class="advanced_area_div adv4">
            <select id="advanced_area_2<?php echo $dropdown_id; ?>" name="advanced_area"  class="cd-select">
                <option value="all" data-parentcity="*" ><?php _e('All Areas','wpestate'); ?></option>
                <?php echo $select_area; ?>
            </select>
        </div>

        <div class="adv_search_internal">
            <input type="text" id="adv_rooms_search" name="advanced_rooms" placeholder="<?php _e('Type Rooms No.','wpestate');?>"      class="advanced_select">
        </div>

        <div class="adv_search_internal"> 
            <input type="text" id="adv_bath_search"  name="advanced_bath"  placeholder="<?php _e('Type Bathrooms No.','wpestate');?>"  class="advanced_select">
        </div>

        <div class="adv_search_internal ">
            <input type="text" id="price_low_search" name="price_low"  class="advanced_select" placeholder="<?php _e('Type Min. Price','wpestate');?>"/>
        </div>

        <div class="adv_search_internal">    
            <input type="text" id="price_max_search" name="price_max"  class="advanced_select" placeholder="<?php _e('Type Max. Price','wpestate');?>"/>
        </div>    

        <?php
        }
        ?>
        

        <input name="submit" type="submit" class="btn vernil small" id="advanced_submit_3" value="<?php _e('Search','wpestate');?>">

    </form>   
</div>  
        

<div id="adv-search-header-contact-3" class="adv3_close <?php echo $hide_class;?>" ><?php _e('Contact Us','wpestate');?></div>
<div id="adv-contact-3" class="adv_body_closed <?php echo $hide_class;?>" >
    <div id="replay_area"></div>
    <div class="adv_search_internal_full" id="adv_search_internal_contact_name">
        <input name="contact_name" id="adv_contact_name" type="text" class="advanced_select"  placeholder="<?php _e('Your Name', 'wpestate'); ?>"  aria-required="true">
    </div>
    
    <div class="adv_search_internal" id="adv_search_internal_email">
        <input type="text" name="email" id="adv_email" aria-required="true"  class="advanced_select" placeholder="<?php _e('Your Email', 'wpestate'); ?>" >                               
    </div>
    
    <div class="adv_search_internal" id="adv_search_internal_phone">
         <input type="text" name="phone" id="adv_phone" class="advanced_select"  placeholder="<?php _e('Your Phone', 'wpestate'); ?>" >
    </div>
    
    <div class="adv_search_internal_full" id="adv_search_internal_full_adv_comment">
         <textarea id="adv_comment" name="comment" cols="45" rows="8" class="advanced_select" placeholder="<?php _e('Your Message', 'wpestate'); ?>"></textarea>
    </div>
    <input type="hidden" name="contact_ajax_nonce" id="contact_ajax_nonce"  value="<?php echo wp_create_nonce( 'ajax-contact' );?>" />
    <input name="submit" type="submit" class="btn vernil small" id="adv_contact_submit" value="<?php _e('Send Message','wpestate');?>">

</div>


        
