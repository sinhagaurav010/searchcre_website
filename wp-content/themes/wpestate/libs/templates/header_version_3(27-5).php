<?php /*<div class="header_social header_social_v3">
   <?php
   $social_facebook    =  esc_html( get_option('wp_estate_facebook_link','') );
   $social_tweet       =  esc_html( get_option('wp_estate_twitter_link','') );
   $social_google      =  esc_html( get_option('wp_estate_google_link','') );

   if($social_facebook!='')  echo '<a href="'.$social_facebook.'" class="social_facebook" target="_blank"></a>';
   if($social_tweet!='')     echo '<a href="'.$social_tweet.'" class="social_tweet" target="_blank"></a>';
   if($social_google!='')    echo '<a href="'.$social_google.'" class="social_google" target="_blank"></a>';
    
     $header_margin_top =   intval ( get_option('wp_estate_header_margin_top',''));
    
   
    if ($header_margin_top!=''){
        $header_margin    = ' style="padding-top:'.$header_margin_top.'px;" ';
    }else{
        $header_margin    = '';
    }
    
   ?>    
</div>
*/?>
<div class="header_control header_control_v3" style="max-width:100% !important;">
    <div class="logo">
            <a href="<?php echo home_url();?>">
                <?php 
                    $logo=get_option('wp_estate_logo_image','');
                    if ( $logo!='' ){ ?>
                        <img src="<?php print $logo;?>" alt="logo" <?php echo $header_margin;?>/>	
                <?php }else{?>
                        <img src="<?php print get_template_directory_uri(); ?>/images/logo.png" alt="logo" <?php echo $header_margin;?> />
                <?php }?>
            </a>
    </div>
<?php if(is_front_page()){?>
    <nav id="access" role="navigation" style="margin-top:20px;margin-right:20px;">
<?php }else {?>
<nav id="access" role="navigation">
<?php }?>
	 <div class="menu-main-menu-container">
	 <ul>
        <?php if(!is_front_page()){?><?php wp_nav_menu( array( 'theme_location' => 'primary','container' => false,'items_wrap'=>'%3$s') ); ?>
		<?php }?>
<?php if(is_front_page()){?>
		<li style="color:#fff;padding-top:10px;">
<img src="<?php print get_template_directory_uri(); ?>/images/tablet-applications.png" alt="menu" style="height:15px;" <?php echo $header_margin;?> />
Download App</li>

<li style="color:#fff;padding-left:50px;padding-right:50px;"><a href="<?php echo get_home_url(); ?>/?page_id=4841" style="color:#fff;border:1px #fff solid;padding:10px 20px 10px 20px;border-radius:100px;">Sell / Rent Property</a></li>

<li class="menu-item menu-item-type-post_type menu-item-object-page nav-li-IIII"><a href="#" onclick="openNav()">
<img src="<?php print get_template_directory_uri(); ?>/images/bars.png" alt="menu" <?php echo $header_margin;?> /><br/>Menu
</a></li>


<?php } else {?>



		<li class="menu-item menu-item-type-post_type menu-item-object-page nav-li-IIII" style="margin-right:10px;margin-top:-10px;"><a href="#" onclick="openNav()" style="color:#000 !important;">
<img src="<?php print get_template_directory_uri(); ?>/images/list-menu.png" alt="menu" <?php echo $header_margin;?> /><br/>Menu
</a></li><?php  } ?>

</ul>
</div>
<script>
function openNav() {
    document.getElementById("mySidenav").style.width = "250px";
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}
</script>

		
		<?php 
global $current_user;
$user_roles = $current_user->roles[0];
if($user_roles == 'contributor' || $user_roles == 'administrator' ){
  //top_bar_front_end_menu();  
  //wp_nav_menu( array( 'theme_location' => 'primary' ) );
}
		 ?>

    </nav><!-- #access -->
	
</div>