<?php
// Template Name: Advanced Results Booking
// Wp Estate Pack
get_header();

global $current_user;
get_currentuserinfo();
$options            =   sidebar_orientation($post->ID);

// get curency,curency position and no of listing per page
$area_array =   $city_array =   $action_array   =   $categ_array    ='';
$pages = get_pages(array(
    'meta_key' => '_wp_page_template',
    'meta_value' => 'compare-prop.php'
     ));

if( $pages ){
    $compare_submit = get_permalink( $pages[0]->ID);
}else{
    $compare_submit = '';
}

$currency           =   esc_html( get_option('wp_estate_currency_symbol', '') );
$where_currency     =   esc_html( get_option('wp_estate_where_currency_symbol', '') );
$prop_no            =   intval ( get_option('wp_estate_prop_no', '') );
$show_compare_link  =   'yes';
$userID             =   $current_user->ID;
$user_option        =   'favorites'.$userID;
$curent_fav         =   get_option($user_option);
$prop_no            =   intval( get_option('wp_estate_prop_no', '') );
$rental_module_status       =   esc_html ( get_option('wp_estate_enable_rental_module','') );

?>

<!-- Google Map Code -->
<?php 
get_template_part('libs/templates/map-template'); 
?> 
<!-- Google Map Code -->


<div id="wrapper" class="<?php print $options['fullwhite']; ?>">  
    <div class="<?php print $options['add_back']; ?>"></div>

    <?php
    print breadcrumb_container($options['full_breadcrumbs'],$options['bread_align'] )
    ?>
    <div id="main" class="row <?php print $options['sidebar_status']; ?>">
    <?php
    print display_breadcrumbs( $options['full_breadcrumbs'] ,$options['bread_align_internal'] )
    ?>


        <!-- begin content--> 
        <div id="post" class="listingborder <?php print $options['grid'].' '.$options['shadow']; ?>"> 
            <div class="inside_post  bottom-estate_property">
               
                    <?php $counter = 0;
                        $custom_advanced_search= get_option('wp_estate_custom_advanced_search','');
                        $meta_query     =   array();
                        $allowed_html   =   array();
                        $book_from      =   '';
                        $book_to        =   '';
                        if( isset($_POST['check_in'])){
                            $book_from      =   wp_kses ( $_POST['check_in'],$allowed_html);
                        }
                        if( isset($_POST['check_out'])){
                            $book_to        =   wp_kses ( $_POST['check_out'],$allowed_html);
                        }
                      
                     
                        //////////////////////////////////////////////////////////////////////////////////////
                        ///// city +  area filters  
                        //////////////////////////////////////////////////////////////////////////////////////

                        if (isset($_POST['booking_location']) && $_POST['booking_location'] != '' ) {
                            $taxcity[] =  $taxarea[] = sanitize_title ( sanitize_text_field($_POST['booking_location']) );
                            $city_array = array(
                                'taxonomy' => 'property_city',
                                'field' => 'slug',
                                'terms' => $taxcity
                            );

                            $area_array = array(
                                'taxonomy' => 'property_area',
                                'field' => 'slug',
                                'terms' => $taxarea 
                            );
                        }

                   

                        //////////////////////////////////////////////////////////////////////////////////////
                        /////booking_guest
                        //////////////////////////////////////////////////////////////////////////////////////

                         $price_max='';
                         if( isset($_POST['booking_guest'])  && is_numeric($_POST['booking_guest']) ){
                             $booking_guest       = intval($_POST['booking_guest']);
                             $price['key']        = 'guest_no';
                             $price['value']      = $booking_guest;
                             $price['type']       = 'NUMERIC';
                             $price['compare']    = '>='; 


                             $meta_query[] = $price;
                         }

                   
                        //////////////////////////////////////////////////////////////////////////////////////
                        ///// compose query 
                        //////////////////////////////////////////////////////////////////////////////////////
                            $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                            if($paged>1){
                               
                               $meta_query= get_option('wpestate_pagination_meta_query','');
                               $categ_array= get_option('wpestate_pagination_categ_query','');
                               $action_array= get_option('wpestate_pagination_action_query','');
                               $city_array= get_option('wpestate_pagination_city_query','');
                               $area_array=get_option('wpestate_pagination_area_query','');
                            }else{
                                update_option('wpestate_pagination_meta_query',$meta_query);
                                update_option('wpestate_pagination_categ_query',$categ_array);
                                update_option('wpestate_pagination_action_query',$action_array);
                                update_option('wpestate_pagination_city_query',$city_array);
                                update_option('wpestate_pagination_area_query',$area_array);
                        
                            }
                            
                          
                            $args = array(
                                'post_type'         =>  'estate_property',
                                'post_status'       =>  'publish',
                                'paged'             =>  $paged,
                                'posts_per_page'    =>  $prop_no,
                                'meta_key'          =>  'prop_featured',
                                'orderby'           =>  'meta_value',
                                'order'             =>  'DESC',
                                'meta_query'        =>  $meta_query,
                                'tax_query'         =>  array(
                                                            'relation' => 'OR',
                                                            $city_array,
                                                            $area_array
                                                        )
                             );

                            $mapargs = array(
                                'post_type'         =>  'estate_property',
                                'post_status'       =>  'publish',
                                'nopaging'          =>  true,
                                'meta_query'        =>  $meta_query,
                                'tax_query'         =>  array(
                                                            'relation' => 'OR',
                                                            $city_array,
                                                            $area_array
                                                        )
                            );
                
                
                
                
                

                       //print_r($args);
                        $prop_selection = new WP_Query($args);
                       
                        $num = $prop_selection->found_posts;
                        $selected_pins=custom_listing_pins($mapargs);//call the new pins
                        ?>

                        <?php while (have_posts()) : the_post(); ?>
                        <?php if (esc_html( get_post_meta($post->ID, 'page_show_title', true) ) == 'yes') { ?>
                        <h1 class="entry-title title_prop"> <?php _e('Your Search Results', 'wpestate'); print " (".$num.")"; ?></h1>
                        <?php }?>
                        <?php the_content(); ?>
                        <?php endwhile; // end of the loop.  ?>  
                    
                    
                    
                    

                    <!--Compare Starts here-->     
                    <div class="prop-compare">
                        <form method="post" id="form_compare" action="<?php print $compare_submit; ?>">
                            <span class="comparetile"><?php _e('Compare properties','wpestate');?></span>
                            <div id="submit_compare"></div>
                         </form>
                    </div>    
                    <!--Compare Ends here-->     
                             <div id="listing_loader">
                                Loading...
                             </div>
                        <div id="listing_ajax_container"> 
                    <!--Listings starts here -->     
                
                
                    <?php  
                    if ($prop_selection->have_posts()){    
                        while ($prop_selection->have_posts()): $prop_selection->the_post();
                          
                        
                                if( wpestate_check_booking_valability($book_from,$book_to,$post->ID) ){
                                    include(locate_template('prop-listing-booking.php'));
                                }
                         
                           
                         
                        endwhile;
                    }else{   
                        print '<div class="bottom_sixty">';
                        _e('We didn\'t find any results. Please try again with different search parameters. ','wpestate');
                        print '</div>';
                    }
                    wp_reset_query(); wp_reset_postdata();
                    ?>
                
                 </div>    
            </div> <!-- end inside post-->
            <?php   kriesi_pagination($prop_selection->max_num_pages, $range =2); ?>    
           
        </div>
        <!-- end content-->

       <?php  include(locate_template('customsidebar.php')); ?>

    </div><!-- #main -->    
</div><!-- #wrapper -->


<?php
wp_localize_script('googlecode_regular', 'googlecode_regular_vars2', 
                       array(  
                           'markers2'           =>  $selected_pins,
                        )
                   );

get_footer(); 


function wpestate_check_booking_valability($book_from,$book_to,$listing_id){
      
    $reservation_array  =   get_booking_dates_advanced_search($listing_id);
    //   print_r($reservation_array);
    $from_date          =   new DateTime($book_from);
    $from_date_unix     =   $from_date->getTimestamp();
    $to_date            =   new DateTime($book_to);
    $to_date_unix       =   $to_date->getTimestamp();
    
    while ($from_date_unix < $to_date_unix){
        $from_date->modify('tomorrow');
        $from_date_unix =   $from_date->getTimestamp();
      //  print'check:'.$from_date_unix.'</br>';
        if( in_array($from_date_unix,$reservation_array ) ){
        //    print 'FOUND OEN';
            return false;
        }
    }
    return true;
}





function get_booking_dates_advanced_search($listing_id){
    $reservation_array  =   array();

    $args=array(
        'post_type'        => 'wpestate_booking',
        'post_status'      => 'any',
        'posts_per_page'   => -1,
        'meta_query' => array(
                            array(
                                'key'       => 'booking_id',
                                'value'     => $listing_id,
                                'type'      => 'NUMERIC',
                                'compare'   => '='
                            ),
                            array(
                                'key'       =>  'booking_status',
                                'value'     =>  'confirmed',
                                'compare'   =>  '='
                            )
                        )
        );
    
  
    $booking_selection  =   new WP_Query($args);

     

     
        foreach ( $booking_selection->posts as $post ) {
         //   $pid            =   get_the_ID();
            $fromd          =   esc_html(get_post_meta($post->ID, 'booking_from_date', true));
            $tod            =   esc_html(get_post_meta($post->ID, 'booking_to_date', true));

            $from_date      =   new DateTime($fromd);
            $from_date_unix =   $from_date->getTimestamp();
            $to_date        =   new DateTime($tod);
            $to_date_unix   =   $to_date->getTimestamp();
            $reservation_array[]=$from_date_unix;

            while ($from_date_unix < $to_date_unix){
                $from_date->modify('tomorrow');
                $from_date_unix =   $from_date->getTimestamp();
                $reservation_array[]=$from_date_unix;
            }
        }
    
         
  
    return $reservation_array;
    
}



?>