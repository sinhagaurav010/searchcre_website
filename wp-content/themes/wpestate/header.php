<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

<title><?php
        // Print the <title> tag based on what is being viewed
	global $page, $paged;
	wp_title( '|', true, 'right' );

	// Add the blog name.
	bloginfo( 'name' );

	// Add the blog description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		echo " | $site_description";

	// Add a page number if necessary:
	if ( $paged >= 2 || $page >= 2 )
		echo ' | ' . sprintf( __( 'Page %s', 'wpestate' ), max( $paged, $page ) );

	?>
</title>

<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
 


<?php 
$favicon        =   esc_html( get_option('wp_estate_favicon_image','') );
$geo_status     =   esc_html ( get_option('wp_estate_geolocation','') );

if ( $favicon!='' ){
    echo '<link rel="shortcut icon" href="'.$favicon.'" type="image/x-icon" />';
} else {
    echo '<link rel="shortcut icon" href="'.get_template_directory_uri().'/images/favicon.gif" type="image/x-icon" />';
}


wp_head();
?>
</head>


<body <?php body_class(); ?>>
<?php
global $current_user;
 $user_roles = $current_user->roles[0];
  ?>
  
</div>


<div id="page">

  <?php //if($user_roles!='subscriber'){
    //top_bar_front_end_menu(); 
  //}	
    $header_type_status     =   intval ( get_option('wp_estate_header_type',''));  ?>  
    <?php if(is_front_page()){?>
	<header id="branding" role="banner" class="branding_version_<?php echo $header_type_status;?>">
	<?php } else {?>
	 <header id="branding" role="banner" class="branding_version_<?php echo $header_type_status;?>">
        
        <?php   
	}
        
         $header_version="header_version_".$header_type_status;
         get_template_part('libs/templates/'.$header_version);
        ?>
       
      
        <?php 
        $geo_status     =   esc_html ( get_option('wp_estate_geolocation','') );
        $custom_image   =   '';
        $rev_slider     =   '';
        if( isset($post->ID) ){
            $custom_image   =   esc_html( esc_html(get_post_meta($post->ID, 'page_custom_image', true)) );  
            $rev_slider     =   esc_html( esc_html(get_post_meta($post->ID, 'rev_slider', true)) );
        }

        $show_adv_search_status     =   get_option('wp_estate_show_adv_search','');
        $hidden_class               =   ' geo_version_'.$header_type_status;
        
        if($geo_status=='yes' && $custom_image=='' && $rev_slider==''){
               if( get_post_type() === 'estate_property' && !is_tax() &&!is_search() ){
                   $hidden_class="geohide xhide";
               }
            
            ?>
        
            <div class="geolocation-button <?php print $hidden_class; ?>" id="geolocation-button"></div> 
            <div id="tooltip-geolocation"  class="tooltip-geolocation_<?php echo $header_type_status;?>"><?php _e('place me on the map','wpestate');?> </div>
           
            <?php  
           
            } 
       
            if( !is_page_template('contact-page.php') ){
                 if( $show_adv_search_status=='yes' or ( ($custom_image=='' && $rev_slider=='') ) ){
                      get_template_part('libs/templates/map-search-form');   
                 }         
            }
            
            ?>
    </header><!-- #branding -->
	<style>
	#advanced_search_map_button {
		display:none !important;
	}
	#search_map_button {
		display:none !important;
	}
	</style>
	<style>
body {
    font-family: "Lato", sans-serif;
}


@media screen and (max-height: 450px) {
  .sidenav {padding-top: 15px;}
  .sidenav a {font-size: 18px;}
}
</style>

