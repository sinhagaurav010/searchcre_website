<?php
// Property Page
// Wp Estate Pack
get_header();
$options                    =   sidebar_orientation($post->ID);
$slider_type                =   get_post_meta($post->ID, 'prop_slider_type', true);
$gmap_lat                   =   esc_html( get_post_meta($post->ID, 'property_latitude', true));
$gmap_long                  =   esc_html( get_post_meta($post->ID, 'property_longitude', true));

$unit                       =   esc_html( get_option('wp_estate_measure_sys', '') );
$currency                   =   esc_html( get_option('wp_estate_submission_curency', '') );
$rental_module_status       =   esc_html ( get_option('wp_estate_enable_rental_module','') );

if (function_exists('icl_translate') ){
    $where_currency             =   icl_translate('wpestate','wp_estate_where_currency_symbol', esc_html( get_option('wp_estate_where_currency_symbol', '') ) );
    $property_description_text  =   icl_translate('wpestate','wp_estate_property_description_text', esc_html( get_option('wp_estate_property_description_text') ) );
    $property_details_text      =   icl_translate('wpestate','wp_estate_property_details_text', esc_html( get_option('wp_estate_property_details_text') ) );
    $property_features_text     =   icl_translate('wpestate','wp_estate_property_features_text', esc_html( get_option('wp_estate_property_features_text') ) );
}else{
    $where_currency             =   esc_html( get_option('wp_estate_where_currency_symbol', '') );
    $property_description_text  =   esc_html( get_option('wp_estate_property_description_text') );
    $property_details_text      =   esc_html( get_option('wp_estate_property_details_text') );
    $property_features_text     =   esc_html( get_option('wp_estate_property_features_text') );
}

$agent_id                   =   '';
$content                    =   '';

global $current_user;
get_currentuserinfo();
$userID                     =   $current_user->ID;
$user_option                =   'favorites'.$userID;
$curent_fav                 =   get_option($user_option);
$favorite_class='isnotfavorite'; 
$favorite_text=__('add to favorites','wpestate');

if($curent_fav){
    if ( in_array ($post->ID,$curent_fav) ){
    $favorite_class='isfavorite';     
    $favorite_text=__('favorite','wpestate');
    } 
}
    

 
global $feature_list_array;


?>
<?php
while (have_posts()) : the_post();
    $image_id   = get_post_thumbnail_id();
    $image_url  = wp_get_attachment_image_src($image_id, 'property_full_map');
    $full_img   = wp_get_attachment_image_src($image_id, 'full');
    $image_url  = $image_url[0];
    $full_img   = $full_img [0];
    $attachment = get_post($image_id);
    $alt_title  =    $attachment->post_excerpt;
    ?>

    <!-- Google Map -->
    <div class="gmap_wrapper" id="gmap_wrapper" data-post_id="<?php echo $post->ID;?>" data-cur_lat="<?php echo $gmap_lat;?>" data-cur_long="<?php echo $gmap_long;?>"  >
        <?php
           
            if($slider_type!='small slider' && $slider_type!=2 && $slider_type!='2' ){ ?>
            <div id="pictureMap">   
                <a href="<?php print $full_img; ?>"  title="<?php print $attachment->post_excerpt;?>" id="preview_link" rel="prettyPhoto[pp_gal]" >
                    <img id="image_full" src="<?php print $image_url; ?>" alt="<?php print $alt_title  ?>">
                </a>
               <?php   
               //data-pretty="prettyPhoto"
                 $arguments = array( 
                                    'numberposts' => -1,
                                    'post_type' => 'attachment', 
                                    'post_parent' => $post->ID,
                                    'post_status' => null,
                                    'exclude' => get_post_thumbnail_id(),
                                    'orderby' => 'menu_order',
                                    'order' => 'ASC'
                                );
                                $post_attachments = get_posts($arguments);

                               
                                foreach ($post_attachments as $attachment) {
                                    $preview = wp_get_attachment_image_src($attachment->ID, 'property_thumb');                       
                                    $full_img = wp_get_attachment_image_src($attachment->ID, 'property_full_map');
                                    $original = wp_get_attachment_image_src($attachment->ID, 'full');
                                    print '<a href="' . $original[0] . '"  style="opacity:0;" rel="prettyPhoto[pp_gal]" ><img  src="'.$preview[0].'"   alt="'.$attachment->post_excerpt.'"></a>';
                                 }
                
                ?>
                
                <div class="image_loader"></div>
            </div>  
        
            <div class="gmap-next-picture" id="gmap-next-picture"></div>
            <div class="gmap-prev-picture" id="gmap-prev-picture"></div>
            <div class="gmap-next" id="gmap-next" style="display:none;"></div>
            <div class="gmap-prev" id="gmap-prev" style="display:none;"></div>
        <?php }else{  ?>
            <div class="gmap-next" id="gmap-next" style="display:block;"></div>
            <div class="gmap-prev" id="gmap-prev" style="display:block;" ></div>
        <?php }?>
     


        <?php 
        $geo_status     =   esc_html ( get_option('wp_estate_geolocation','') );
        

        if($geo_status=='yes'){ 
          print'  <div id="mobile-geolocation-button" style="display:none;"></div>';
        } 
        ?>
         
        <div id="googleMap">   
        </div>    
        
         <div id="gmap-loading"><?php _e('Loading Maps','wpestate');?>
       <span class="gmap-animation">
            <img src="<?php print get_template_directory_uri(); ?>/images/ajax-loader-gmap.gif" alt="logo"/>
       </span>
        </div>
        
    </div>       
    <!-- END Google Map -->   




    <div id="wrapper" class="<?php print $options['fullwhite']; ?>">  
        <div class="<?php print $options['add_back']; ?>" style="background-color:#f7f7f7;"></div>

        <?php
        print breadcrumb_container($options['full_breadcrumbs'],$options['bread_align'],$post->ID )
        ?>
        <div id="main" class="row <?php print $options['sidebar_status']; ?>" style="background: #f7f7f7;">
        <?php
        print display_breadcrumbs( $options['full_breadcrumbs'] ,$options['bread_align_internal'],$post->ID )
        ?>


<style>
.sixprop h4, .under-title, .property_price, .listingborder a:hover{color: #005375;
    font-weight: 400;}
</style>
            <!-- begin content--> 
            <div id="post" class=" twelve columns alpha shadowonleft" style="box-shadow: none;background:none;padding-top:30px;"> 
                <div class="inside_post inside_no_border" style="background:#ffffff;padding: 20px 10px 10px 20px;border:1px #eaeaea solid;">

                    <?php get_template_part( 'libs/templates/property-anchor-menu');?>

                    <?php                
                    $google_view = esc_html(get_post_meta($post->ID, 'property_google_view', true));                 
                    $pin=get_the_terms($post->ID, 'property_action_category');
                    $pinclass='';
                    if($pin!=''){
                        foreach ( $pin as $term ) {  
                            $pinclass=$term->slug.'pin';
                        } 
                    }
                   
                  
				
                    include(locate_template('libs/templates/thumbrow.php'));

                    $property_address   = esc_html( get_post_meta($post->ID, 'property_address', true) );
                    $property_city      = get_the_term_list($post->ID, 'property_city', '', ', ', '');
                    $property_area      = get_the_term_list($post->ID, 'property_area', '', ', ', '');
                    $property_county    = esc_html( get_post_meta($post->ID, 'property_county', true) );
                    $property_state     = esc_html(get_post_meta($post->ID, 'property_state', true) );
                    $property_zip       = esc_html(get_post_meta($post->ID, 'property_zip', true) );
                    $property_country   = esc_html(get_post_meta($post->ID, 'property_country', true) );
                    $property_size      = intval(get_post_meta($post->ID, 'property_size', true) );
                    
                    
                    if ($property_size  != '') {
                        $property_size  = number_format($property_size) . ' '.__('square','wpestate').' ' . $unit;
                    }

                    $property_lot_size = intval( get_post_meta($post->ID, 'property_lot_size', true) );
                    if ($property_lot_size != '') {
                        $property_lot_size = number_format($property_lot_size) . ' '.__('square','wpestate').' ' . $unit;
                    }

                    $property_rooms                 = floatval ( get_post_meta($post->ID, 'property_rooms', true) );
                    $property_bedrooms              = floatval ( get_post_meta($post->ID, 'property_bedrooms', true) );
                    $property_bathrooms             = floatval ( get_post_meta($post->ID, 'property_bathrooms', true) );                                  
                   
                    if($rental_module_status=='yes'){
                        $price_per            = floatval ( get_post_meta($post->ID, 'price_per', true) );  
                        $guest_no             = floatval ( get_post_meta($post->ID, 'guest_no', true) );
                        $cleaning_fee         = floatval ( get_post_meta($post->ID, 'cleaning_fee', true) ); 
                        $city_fee             = floatval ( get_post_meta($post->ID, 'city_fee', true) ); 
                   }
                    
                    ?>
                    <div class="blankpin <?php print $pinclass;?>"></div>
                    
                    
                    
                    <!-- start  sixprop content container-->
                    <div class="sixprop propcol">
                        <h1 class="entry-title-prop"><?php the_title(); ?></h1>
                        <div class="under-title">
                            <?php
                            $price       = intval   ( get_post_meta($post->ID, 'property_price', true) );
                            $price_label = esc_html ( get_post_meta($post->ID, 'property_label', true) );
                            if ($price != 0) {
                                $price = number_format($price);
                                if ($where_currency == 'before') {
                                    $price          = $currency . ' ' . $price;
                                    if($rental_module_status=='yes'){
                                        $cleaning_fee   = $currency. ' '. $cleaning_fee;
                                        $city_fee       = $currency. ' '. $city_fee;
                                    }                                   
                                } else {
                                    $price          = $price . ' ' . $currency;
                                    if($rental_module_status=='yes'){
                                        $cleaning_fee   = $cleaning_fee. ' '. $currency;
                                        $city_fee       = $currency. ' '. $city_fee;                                       
                                    }
                                }
                                
                                if( $rental_module_status=='yes'){
                                    $price_per_label=wpestate_booking_price_per($price_per);
                                     print $price.' '.$price_per_label;
                                }else{
                                    print $price.' '.$price_label;
                                }
                              
                            }else{
                                $price='';
                            }
                            
                            ?>
                            <?php
                         
                            
                            $status = esc_html( get_post_meta($post->ID, 'property_status', true) );    
                            if (function_exists('icl_translate') ){
                                $status     =   icl_translate('wpestate','wp_estate_property_status_'.$status, $status ) ;                                      
                            }
        
                            if ($status != 'normal') {
                                print ' - ' . $status;
                            }
                            ?>
                        </div>
                        
                     
                       
                        
                        
                                
                                
                        <div class="under-title-addres" id="prop_des">
                            <?php  print esc_html( get_post_meta($post->ID, 'property_address', true) ). ', ' . get_the_term_list($post->ID, 'property_city', '', ', ', ''); ?>
                        </div>
                        
                        <?php
                        if( $rental_module_status=='yes'){
                            if($guest_no!='' || $guest_no!=0 || $cleaning_fee!='' || $cleaning_fee!=0 || $city_fee!='' || $city_fee!=0 ){
                                print '<div class="under_title_reservation">';
                                
                                if($guest_no!='' || $guest_no!=0 ){
                                    print '<span class="under_title_span">'.__('Maximum Guest No:').' '.$guest_no.'</span>';
                                }
                                if($cleaning_fee!='' || $cleaning_fee!=0 ){
                                    print '<span class="under_title_span">'.__('Cleaning Fee:').' '.$cleaning_fee.'</span>';
                                }
                                if($city_fee!='' || $city_fee!=0 ){
                                    print '<span class="under_title_span">'.__('City fee:').' '.$city_fee.'</span>';
                                }
                                
                                print '</div>';
                            }
                        }
                             
                        ?>

                        <div class="mobilenav">
                            <span class="prev_prop"><?php previous_post_link('%link', ''); ?></span>
                            <span class="next_prop"><?php next_post_link('%link', ''); ?></span>
                        </div> 
                        
              
                        
                        <?php
                        if($slider_type=='small slider' || $slider_type=='2' || $slider_type==2){
                             include(locate_template('libs/templates/property-slider.php'));
                        }
                          
                         
                        ?>
                        
                        <?php
                        $content = get_the_content();
                        $content = apply_filters('the_content', $content);
                        $content = str_replace(']]>', ']]&gt;', $content);
                        if($content!=''){
                            print'<div class="property_description">';
                            if($property_description_text==''){
                              print '<h4 >' .__('Property Description', 'wpestate').'</h4>';
                            }else{
                              print '<h4>'.$property_description_text.'</h4>';  
                            }
                            print $content;
                            print '</div>';
                            print ' <hr class="dottedline">';
                        }
                        
                       
                        // booking reservation functions
                     
                        if( $rental_module_status=='yes'):
                            //show_booking_calendar();
                            show_booking_reservation();
                        endif;
                        ?>
                        
                        <h4 id="prop_det">
                        <?php  
                        if($property_details_text=='') {
                            _e('Property Details', 'wpestate');
                        }else{
                            print $property_details_text;
                        }
                        ?>
                        </h4>
                        <div class="prop_details">
                            <ul>
                                <?php 
                             
                                if ($price !='' ){ 
                                    print'  <li><span class="title_feature_listing">'.__('Price','wpestate');  print ': </span> ';
                                   if( $rental_module_status=='yes'){
                                       print $price.' '.$price_per_label; 
                                    }else{
                                        print $price.' '.$price_label;
                                    }
                                    print '</li>';
                                }
                                
                                if ($property_address != ''){
                                    print '<li><span class="title_feature_listing">'.__('Address','wpestate').': </span>' . $property_address . '</li>'; 
                                }
                                if ($property_city != ''){
                                    print '<li><span class="title_feature_listing">'.__('City','wpestate').': </span>' .$property_city. '</li>';  
                                }  
                                if ($property_area != ''){
                                     print '<li><span class="title_feature_listing">'.__('Area','wpestate').': </span>' .$property_area. '</li>';
                                }    
                                if ($property_county != ''){
                                    print '<li><span class="title_feature_listing">'.__('County','wpestate').': </span>' . $property_county . '</li>'; 
                                }
                                if ($property_state != ''){
                                    print '<li><span class="title_feature_listing">'.__('State','wpestate').': </span>' . $property_state . '</li>'; 
                                }
                                if ($property_zip != ''){
                                    print '<li><span class="title_feature_listing">'.__('Zip','wpestate').': </span>' . $property_zip . '</li>';
                                }  
                                if ($property_country != '') {
                                    print '<li><span class="title_feature_listing">'.__('Country','wpestate').': </span>' . $property_country . '</li>'; 
                                } 
                                
                                ?>
                            </ul>
                        </div>    



                        <div class="prop_details">
                            <ul>
                        
                                <?php 
                                if ($property_size != ''){
                                    print '<li><span class="title_feature_listing">'.__('Property Size','wpestate').': </span>' . $property_size . '</li>';
                                }               
                                if ($property_lot_size != ''){
                                    print '<li><span class="title_feature_listing">'.__('Property Lot Size','wpestate').': </span>' . $property_lot_size . '</li>';
                                }      
                                if ($property_rooms != ''){
                                    print '<li><span class="title_feature_listing">'.__('Rooms','wpestate').': </span>' . $property_rooms . '</li>'; 
                                }      
                                if ($property_bedrooms != ''){
                                    print '<li><span class="title_feature_listing">'.__('Bedrooms','wpestate').': </span>' . $property_bedrooms . '</li>'; 
                                }     
                                if ($property_bathrooms != '')    {
                                    print '<li><span class="title_feature_listing">'.__('Bathrooms','wpestate').': </span>' . $property_bathrooms . '</li>'; 
                                }      
                                if( $rental_module_status=='yes'){
                                    if($guest_no!='' || $guest_no!=0)  {
                                        print '<li><span class="title_feature_listing">'.__('Maximum Guests No','wpestate').': </span>' . $guest_no . '</li>'; 
                                    }      
                                }
                                 ?>
                            </ul> 
                        </div> 
                        
                        <!--Custom Properties -->
                        <div class="prop_details_custom">
                            <?php 
                            $i=0;
                            $custom_fields = get_option( 'wp_estate_custom_fields', true); 
                            
                            if( !empty($custom_fields) ){
                                while($i< count($custom_fields) ){
                                   $name =   $custom_fields[$i][0];
                                   $label=   $custom_fields[$i][1];
                                   $type =   $custom_fields[$i][2];
                                   $slug =   str_replace(' ','_',$name);
                                    
                                   if($type=='numeric'){
                                        $value=floatval(get_post_meta($post->ID, $slug, true));                                       
                                   }else{
                                        $value=esc_html(get_post_meta($post->ID, $slug, true));
                                   }
                                   
                                   
                                   if (function_exists('icl_translate') ){
                                         $label     =   icl_translate('wpestate','wp_estate_property_custom_'.$label, $label ) ;
                                         $value     =   icl_translate('wpestate','wp_estate_property_custom_'.$value, $value ) ;                                      
                                   }

                                   if($value!=''){
                                          print  '<p><span class="title_feature_listing">'.ucwords($label).': </span>'.$value .'</p>';
                                   }
                                   $i++;       
                                }
                            }
    
                            ?>
                       </div>
                        
                        
                        
                        
                        
                        
                        
         
                        <?php 
                        // features and ammenties
                        $counter            =   0;                          
                        $feature_list_array =   array();
                        $feature_list       =   esc_html( get_option('wp_estate_feature_list') );
                        $feature_list_array =   explode( ',',$feature_list);
                        $total_features     =   round( count( $feature_list_array )/2 );
                       
                        if ( !count( $feature_list_array )==0 ){ //  if are features and ammenties
                        ?>
                        <h4 id="prop_ame">
                        <?php 
                        if($property_features_text ==''){
                            //_e('Amenities and Features', 'wpestate'); 
                        }else{
                            //print $property_features_text ;
                        }
                        ?>
                        </h4>
                    
                            <?php 
                            /*$show_no_features= esc_html ( get_option('wp_estate_show_no_features','') );
                            if($show_no_features!='no'){
                                 print'    <div class="prop_details"><ul>';
                                 foreach($feature_list_array as $checker => $value){
                                      
                                        if (function_exists('icl_translate') ){
                                            $value     =   icl_translate('wpestate','wp_estate_property_custom_amm_'.$value, $value ) ;                                      
                                        }
                                
                                
                                        $counter++;
                                        if ($counter == ($total_features+1) ) {
                                            print'</ul></div><div class="prop_details"><ul>';
                                        }
                                        $post_var_name=  str_replace(' ','_', trim($value) );

                                        if (esc_html( get_post_meta($post->ID, $post_var_name, true) ) == 1) {
                                            print '<li><img src="' . get_template_directory_uri() . '/images/yes_prop.png" alt="yes"/>' . trim($value) . '</li>';
                                        }else{
                                            print '<li><img src="' . get_template_directory_uri() . '/images/no_prop.png" alt="no"/>' . trim($value). '</li>';
                                        }
                                  }
                                 print'</ul>     </div>';
                            }else{
                                 foreach($feature_list_array as $checker => $value){
                                    $post_var_name=  str_replace(' ','_', trim($value) );
                                     
                                    if (function_exists('icl_translate') ){
                                        $value     =   icl_translate('wpestate','wp_estate_property_custom_amm_'.$value, $value ) ;                                      
                                    }
                                
                                        
                                    if (esc_html( get_post_meta($post->ID, $post_var_name, true) ) == 1) {
                                        print '<div class="features_listing_div"><img src="' . get_template_directory_uri() . '/images/yes_prop.png" alt="yes"/>' . trim($value) . '</div>';
                                    }
                                }
                           }
                        */?>
                        
                        
                        <?php 
                        } // end if are features and ammenties
                        ?>
                        
                        
        
                        
                        <?php
                        $message = esc_html( get_post_meta($post->ID, 'property_sale_pitch', true) );
                        if ($message != '') {
                            print '
                            <div class="properties_agent_notes">
                            <h4>';
                            _e('Agent Notes', 'wpestate');
                            print '</h4>' . $message.'</div>';
                        }
                        ?>
                            
                        <hr class="dottedline bottom-estate_property">

                        <?php
                        wp_reset_query();
                        if( $rental_module_status != 'yes'){
                            get_template_part ('libs/templates/agent-area');
                        }
               
                        ?>
                              
                               
                        <?php 
                        if( $rental_module_status=='yes'){
                            include(locate_template('similar-listings-booking.php'));
                        }else{
                            include(locate_template('similar-listings.php'));
                        }
                        
                        ?>
                        <span id="menu_stopper"></span>
                        
                      
             
                  
                </div>
                <!-- end sixprop content container-->


                <!-- start fixed sidebar-->
                <?php /*<div class="twoprop propcol">
                    <span class="prev_prop"><?php previous_post_link('%link', ''); ?></span>
                    <span class="next_prop"><?php next_post_link('%link', ''); ?></span>      
                    <div id="add_favorites" class="<?php print $favorite_class;?>" data-postid="<?php the_ID();?>"><?php echo $favorite_text;?></div>                 
                 </div>*/?>
                <!-- end fixed sidebar-->

<?php endwhile; // end of the loop.  ?>


    </div><!-- end inside-->        
</div>
<!-- end content-->




    <?php  //include(locate_template('customsidebar.php')); ?>


</div><!-- #main -->    
</div><!-- #wrapper -->

<?php get_footer(); ?>