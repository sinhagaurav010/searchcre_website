<?php
// Template Name: Properties list
// Wp Estate Pack
get_header();

$options=sidebar_orientation($post->ID);
$filtred=0;

///////////////////// fiind out who is the compare page
$pages = get_pages(array(
    'meta_key' => '_wp_page_template',
    'meta_value' => 'compare-prop.php'
        ));

if( $pages ){
    $compare_submit = get_permalink( $pages[0]->ID);
}else{
    $compare_submit='';
}


// get curency , currency position and no of items per page

global $current_user;
get_currentuserinfo();

$currency                   =   esc_html( get_option('wp_estate_currency_symbol', '') );
$where_currency             =   esc_html( get_option('wp_estate_where_currency_symbol', '') );
$prop_no                    =   intval( get_option('wp_estate_prop_no', '') );
$userID                     =   $current_user->ID;
$user_option                =   'favorites'.$userID;
$curent_fav                 =   get_option($user_option);
$icons                      =   array();
$taxonomy                   =   'property_action_category';
$tax_terms                  =   get_terms($taxonomy);
$taxonomy_cat               =   'property_category';
$categories                 =   get_terms($taxonomy_cat);
//die;
?>
        <div id="post" style="width:100%;    background:linear-gradient(#005375 , #2c90b9) repeat scroll 0% 0%;
}
    padding: 0px 24px 20px;margin-top:-12px;display:inline-block; "> 
            <div class="no_margin_bottom  bottom-estate_property">
                <?php while (have_posts()) : the_post(); ?>
                <?php if (esc_html( get_post_meta($post->ID, 'page_show_title', true) ) == 'yes') { ?>
                   
                <?php } ?>
                <?php the_content(); ?>
                <?php endwhile; // end of the loop.  ?>  
</div>
</div>

<!-- Google Map Code -->
<?php 
//get_template_part('libs/templates/map-template'); 

?> 
<!-- Google Map Code -->


		<link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet" type="text/css" media="all" />
		<link href="http://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css" media="all" />
		<link href="<?php echo get_template_directory_uri();?>/css/normalize.css" rel="stylesheet" type="text/css" media="all" />
		<link href="<?php echo get_template_directory_uri();?>/css/responsive-accordion.css" rel="stylesheet" type="text/css" media="all" />
		<link href="<?php echo get_template_directory_uri();?>/css/style.min.css" rel="stylesheet" type="text/css" media="all" />


<div id="wrapper" class="<?php print $options['fullwhite']; ?>">  

    <div class="<?php print $options['add_back']; ?>"></div>
    
    <?php  print breadcrumb_container($options['full_breadcrumbs'],$options['bread_align'] ); ?>
    <div id="main" class="row <?php print $options['sidebar_status']; ?>" style="max-width:100%;padding:30px;background: #f7f7f7 none repeat scroll 0 0;">
    <?php  //print display_breadcrumbs( $options['full_breadcrumbs'] ,$options['bread_align_internal'] ); ?>



        <!-- begin content--> 
     <?php /*   <div id="post" class=" <?php print $options['grid'].' ' . $options['shadow']; ?> "> 
            <div class="inside_post no_margin_bottom  bottom-estate_property">
                <?php while (have_posts()) : the_post(); ?>
                <?php if (esc_html( get_post_meta($post->ID, 'page_show_title', true) ) == 'yes') { ?>
                    <h1 class="entry-title title_prop"><?php the_title(); ?></h1>
                <?php } ?>
                <?php the_content(); ?>
                <?php endwhile; // end of the loop.  ?>  
*/?>
                <!--Filters starts here-->     
                <?php //get_template_part('libs/templates/property_list_header'); ?> 
                <!--Filters Ends here-->     

                <!--Compare Starts here-->     
                <?php /*<div class="prop-compare">
                    <form method="post" id="form_compare" action="<?php print $compare_submit; ?>">
                        <span class="comparetile"><?php _e('Compare properties','wpestate')?></span>
                         <div id="submit_compare"></div>
                    </form>
                </div>  
*/?>				
                <!--Compare Ends here-->     

                <!-- Listings starts here -->                   
							
                <div id="listing_loader">Loading...</div>
                <div id="listing_ajax_container"> 
<div style="padding:10px;border: 1px solid #ddd;border-bottom:none;background:#FAFAFA;" class="twelve columns alpha nomargin">
			    <div class="one columns alpha nomargin"></div>
				<div style="font-weight:bold;color:#86888a !important;" class="two columns alpha nomargin">Address</div>
				<div style="font-weight:bold;color:#86888a !important;" class="five columns alpha nomargin">City</div>
				<div style="font-weight:bold;color:#86888a !important;" class="two columns alpha nomargin">Type</div>
				<div style="font-weight:bold;color:#86888a !important;" class="two columns alpha nomargin">Price</div>
				<div class="one columns alpha nomargin"></div>
			   </div>
                    <ul class="responsive-accordion responsive-accordion-default bm-larger">  
                    
                <?php
                wp_reset_query();
                $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                if(is_front_page()){
                     $paged = (get_query_var('page')) ? get_query_var('page') : 1;
                }
            
                $args = array(
                    'post_type'         => 'estate_property',
                    'post_status'       => 'publish',
                    'paged'             => $paged,
                    'posts_per_page'    => $prop_no,
                    'meta_key'          => 'prop_featured',
                    'orderby'           => 'meta_value',
                    'order'             => 'DESC',
                    
                    
                );
            //print_r($args);
                add_filter( 'posts_orderby', 'my_order' ); 
                $prop_selection = new WP_Query($args);
                remove_filter( 'posts_orderby', 'my_order' ); 
                $counter = 0;
                $rental_module_status= esc_html ( get_option('wp_estate_enable_rental_module','') );
                while ($prop_selection->have_posts()): $prop_selection->the_post(); 
                   
                    if( $rental_module_status=='yes'){
                        include(locate_template('prop-listing-booking.php'));
                    }else{
                        include(locate_template('prop-listing.php'));
                    }
                   
                endwhile;
                wp_reset_query();               
                ?>
				</ul>
                </div>  
                <!-- Listings Ends  here -->   

 <?php   kriesi_pagination($prop_selection->max_num_pages, $range =2); ?>    
            				
            </div> <!-- end inside post-->
           

        </div> 
        <!-- end content-->




    <!-- begin sidebar -->
    <?php if ($options['sidebar_status'] != 'none' and $options['sidebar_status'] != '') { ?>
    <div id="primary" class="widget-area-sidebar three columns <?php print $options['side']; ?>">  	 	
        <ul class="xoxo">
        <?php generated_dynamic_sidebar($options['sidebar_name']); ?>
        </ul>
    </div>
    <?php } ?> 
    <!-- end sidebar -->




    </div><!-- #main -->    
</div><!-- #wrapper -->
<?php get_footer(); ?>
<style>
.search_wrapper {
	display:none!important;
}
</style>