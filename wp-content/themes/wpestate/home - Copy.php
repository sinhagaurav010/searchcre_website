<?php
// Template Name:Front page
// Wp Estate Pack
get_header();
$options        =   sidebar_orientation($post->ID);
$custom_image   =   esc_html( esc_html(get_post_meta($post->ID, 'page_custom_image', true)) );   


// get page border if any
$border_option = esc_html( get_post_meta($post->ID, 'border_option', true) );
$border='';

switch ($border_option){
    case 'agent border':
        $border='agentborder';
        break;
    case 'listing border':
        $border='listingborder ';
        break;
    case 'blog border':
       $border='blogborder';
        break;
    case 'white border':
       $border='none';
        break;
    case 'no border':
       $border='';
        break;
     case '':
        $border='none';
        break;
}

?>


<!-- Google Map Code -->
<?php 
//get_template_part('libs/templates/map-template'); 
?> 
<!-- Google Map Code -->

<style>
.whiteonleft{display:none;}
.ui-widget-header{background:transparent !important;border:none !important;}
.ui-widget-content{background:transparent !important;border:none !important;}
.adv_search_submit{width:200px !important;}
.ui-tabs .ui-tabs-nav{padding:0px 0px !important;}
.advanced_search_map {
	display:none !important;
}
.greendiv{
	background:green !important;
}
.search_wrapper{
	display:none;
}
.adv_search_internal {
	display:none;
}
</style>
<?php $upload_dir = wp_upload_dir();
$path=$upload_dir['baseurl'];
?>
<div style="height:600px;background:#000 url('<?php echo $path;?>/2016/04/home-bg.png') no-repeat;">

<?php 


$args = array(
        'hide_empty'    => true  
        ); 

$show_empty_city_status= esc_html ( get_option('wp_estate_show_empty_city','') );

if ($show_empty_city_status=='yes'){
    $args = array(
        'hide_empty'    => false  
        ); 
}

//$args=array( 'hide_empty'    => true );
$taxonomy = 'property_action_category';
$tax_terms = get_terms($taxonomy,$args);

$taxonomy_categ = 'property_category';
$tax_terms_categ = get_terms($taxonomy_categ,$args);

                $custom_advanced_search= get_option('wp_estate_custom_advanced_search','');
                $meta_query=array();
                
                if($custom_advanced_search==='yes'){ // we have advanced search
                    //get custom search fields
                    $adv_search_what    = get_option('wp_estate_adv_search_what','');
                    $adv_search_how     = get_option('wp_estate_adv_search_how','');
                    $adv_search_label   = get_option('wp_estate_adv_search_label','');                    
                    $adv_search_type    = get_option('wp_estate_adv_search_type','');
				}
?>
  
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
  
  <script>
  $( document ).ready(function() {
	
	  $('#rental').addClass('greendiv');
	  
	  $('#tclick').val('Rentals');
  $( "#rental" ).click(function() {
	  $('#rental').addClass('greendiv');
	  $('#sales').removeClass('greendiv');
	  var vl= $(this).attr("value");
	  $('.tclick').val(vl);
      
     
});
$( "#sales" ).click(function() {
	  $('#sales').addClass('greendiv');
	  $('#rental').removeClass('greendiv');
	  var vl= $(this).attr("value");
	  $('#tclick').val(vl);
      
     
});
  
});
  </script>
<div style="height: 100%;
    margin: 0 auto;
    max-width: 960px;padding-top:180px;">
	
	
	
			 <form action="http://54.85.15.66/realestate/?page_id=6908" method="post" role="search" style="margin:0 auto;" >
                
				
                <?php
           /*
                    if( !empty( $tax_terms ) ){                       
                        print'<div class="adv_search_checkers firstrow" style="float:left;width:none !important;"><select name="filter_search_action" id="search_'.$limit50.'" class="search_'.$tax_term->name.'">';
                        foreach ($tax_terms as $tax_term) {
                            $icon_name          =   limit64( 'wp_estate_icon'.$tax_term->slug);
                            $limit50            =   limit50( $tax_term->slug);
                            
							print '<option value="'.$tax_term->name.'">'.$tax_term->name.'</option>';
                            if( $icons[$limit50]!='' ){
                                 print '<img src="'.$icons[$limit50].'" alt="'.$tax_term->name.'">' . $tax_term->name . '</label></div>';
                            }else{
                                 print '<img src="'.get_template_directory_uri().'/css/css-images/'.$tax_term->slug.'icon.png" alt="'.$tax_term->name.'">' . $tax_term->name . '</label></div>';
                            }
                        }
                        print '</select></div>';
                    }
                    
             */  
              ?>
               
					
					<div id="tabs">
					<input id="rental" type="Button" value="Rentals" style="background:#000;border:0px;color:#fff;width:100px;height:40px;">
					<input id="sales" type="Button" value="Sales" style="background:#000;border:0px;color:#fff;width:100px;height:40px;">
  <input type="hidden" id="tclick" name="filter_search_action" value=""/>
  
  <div style="clear:both;background:#fff !important;width:300px;">
   
  </div>

</div>
<div style="background-color:green;float:left;padding:8px;">
		
 <?php 	
                    if( !empty( $tax_terms_categ ) ){
						
                        print ' <div class="adv_search_checkers " style="float:left;width:300px !important;margin-left:0px;">
						<select name="filter_search_type" id="search_'.$limit50.'" class="search_' . $limit50. '"  style="width:300px;height:40px;">';
						
                        foreach ($tax_terms_categ as $categ) {
                            $icon_name          =   limit64( 'wp_estate_icon'.$categ->slug);
                            $limit50            =   limit50( $categ->slug);
   
                            print '<option value="'.$categ->name.'">'.$categ->name.'</option>';

                            if( $icons[$limit50]!='' ){
                                print'<img src="'.$icons[$limit50].'" alt="'.$categ->slug.'">' . $categ->name . '</label></div>';
                            }else{
                                print'<img src="'.get_template_directory_uri().'/css/css-images/'.$categ->slug.'icon.png" alt="'.$categ->name.'">' . $categ->name . '</label></div>';
                            }                      
                        }
                        print '</select></div>';
                    }
					?>		
      <input type="text" id="advanced_city" name="advanced_city" placeholder="Enter City" style="float:left;width:300px;height:40px;padding:0px;padding-left:6px;border:none;margin-left:5px;" />          
	  
	  </div>
<?php                    $custom_advanced_search= get_option('wp_estate_custom_advanced_search','');
   
                    if ( $custom_advanced_search == 'yes'){
						
							?>
							

						
<?php                    }else{
                    ?>
                        
               
                
                 <div class="adv_search_internal">
                    <input type="text" id="adv_rooms_search" name="advanced_rooms" placeholder="<?php _e('Type Rooms No.','wpestate');?>"      class="advanced_select">
                    <input type="text" id="adv_bath_search"  name="advanced_bath"  placeholder="<?php _e('Type Bathrooms No.','wpestate');?>"  class="advanced_select">
                 </div>
            
                <div class="adv_search_internal lastadv">
                    <input type="text" id="price_low_search" name="price_low"  class="advanced_select" placeholder="<?php _e('Type Min. Price','wpestate');?>"/>
                    <input type="text" id="price_max_search" name="price_max"  class="advanced_select" placeholder="<?php _e('Type Max. Price','wpestate');?>"/>
                </div>
                
             
                
                
                <?php
                }
                ?>
                
               
                
                
                
                
                
                
                <div class="adv_search_submit" style="float:left;margin-left:5px;">
                    <input name="submit" type="submit" class="btn vernil small" id="advanced_submit" value="<?php _e('Search','wpestate');?>" style="padding: 15px 22px;font-size:16px;">
                </div>
       
     </form>
 <!-- end content-->
</div>


 
 
</div>

<div id="wrapper" class="<?php print $options['fullwhite']; ?>">  

    <div class="<?php print $options['add_back']; ?>"></div>

    <?php
    //print breadcrumb_container($options['full_breadcrumbs'],$options['bread_align'] )
    ?>
    <div id="main" class="row <?php print $options['sidebar_status']; ?>" style="padding-top:40px;padding-bottom:60px;">
    <?php
    
    ?>

        
        <!-- begin content--> 
       
           
            
            <div class="inside_post inside_no_border <?php
            if (is_front_page()) {
                print 'is_home_page';
            }
            ?>" >

                <?php while (have_posts()) : the_post(); ?>
                    <?php if (esc_html( get_post_meta($post->ID, 'page_show_title', true) ) != 'no') { ?>
                        <h1 class="entry-title"><?php //the_title(); ?></h1><br/>
                    <?php } ?>
                    <?php the_content(); ?>


                    <?php endwhile; // end of the loop.  ?>
             <!-- end inside post-->
			 
			


        <?php  //include(locate_template('customsidebar.php')); ?>
        
    </div><!-- #main -->    
</div><!-- #wrapper -->

<?php get_footer(); ?>