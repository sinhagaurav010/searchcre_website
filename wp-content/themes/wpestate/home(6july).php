<?php
// Template Name:Front page
// Wp Estate Pack
get_header();

$options        =   sidebar_orientation($post->ID);
$custom_image   =   esc_html( esc_html(get_post_meta($post->ID, 'page_custom_image', true)) );   


// get page border if any
$border_option = esc_html( get_post_meta($post->ID, 'border_option', true) );
$border='';

switch ($border_option){
    case 'agent border':
        $border='agentborder';
        break;
    case 'listing border':
        $border='listingborder ';
        break;
    case 'blog border':
       $border='blogborder';
        break;
    case 'white border':
       $border='none';
        break;
    case 'no border':
       $border='';
        break;
     case '':
        $border='none';
        break;
}

?>


<!-- Google Map Code -->
<?php 
//get_template_part('libs/templates/map-template'); 
?> 
<!-- Google Map Code -->

<style>
.whiteonleft{display:none;}
.ui-widget-header{background:transparent !important;border:none !important;}
.ui-widget-content{background:transparent !important;border:none !important;}
.adv_search_submit{width:200px !important;}
.ui-tabs .ui-tabs-nav{padding:0px 0px !important;}
.advanced_search_map {
	display:none !important;
}
//.greendiv{
//	background:rgba(0, 0, 0, .4) !important;
//}
.unselected{
	background:rgba(0, 0, 0, .8) !important;
}
.search_wrapper{
	display:none;
}
.adv_search_internal {
	display:none;
}
.small.btn{
	min-width: 98% !important;
}
</style>

<?php $upload_dir = wp_upload_dir();
$path=$upload_dir['baseurl'];
?>
<div style="height:700px;background:#000 url('<?php echo $path;?>/2016/04/home-bg.png') no-repeat scroll 0px 0px;">


 <?php 


$args = array(
        'hide_empty'    => true  
        ); 

$show_empty_city_status= esc_html ( get_option('wp_estate_show_empty_city','') );

if ($show_empty_city_status=='yes'){
    $args = array(
        'hide_empty'    => false  
        ); 
}

//$args=array( 'hide_empty'    => true );
$taxonomy = 'property_action_category';
$tax_terms = get_terms($taxonomy,$args);

$taxonomy_categ = 'property_category';
$tax_terms_categ = get_terms($taxonomy_categ,$args);

                $custom_advanced_search= get_option('wp_estate_custom_advanced_search','');
                $meta_query=array();
                
                if($custom_advanced_search==='yes'){ // we have advanced search
                    //get custom search fields
                    $adv_search_what    = get_option('wp_estate_adv_search_what','');
                    $adv_search_how     = get_option('wp_estate_adv_search_how','');
                    $adv_search_label   = get_option('wp_estate_adv_search_label','');                    
                    $adv_search_type    = get_option('wp_estate_adv_search_type','');
				}
?>
  
<div class="row " id="main"  style="height: 100%;margin: 0 auto;padding-top:220px;">
	<div class="noborder twelve columns alpha" id="post"> 
            <div class="inside_post inside_no_border">
	
			 <form action="<?php echo get_home_url(); ?>/abc/" method="post" role="search" style="margin:0 auto;" >
                
				
                <?php
           /*
                    if( !empty( $tax_terms ) ){                       
                        print'<div class="adv_search_checkers firstrow" style="float:left;width:none !important;"><select name="filter_search_action" id="search_'.$limit50.'" class="search_'.$tax_term->name.'">';
                        foreach ($tax_terms as $tax_term) {
                            $icon_name          =   limit64( 'wp_estate_icon'.$tax_term->slug);
                            $limit50            =   limit50( $tax_term->slug);
                            
							print '<option value="'.$tax_term->name.'">'.$tax_term->name.'</option>';
                            if( $icons[$limit50]!='' ){
                                 print '<img src="'.$icons[$limit50].'" alt="'.$tax_term->name.'">' . $tax_term->name . '</label></div>';
                            }else{
                                 print '<img src="'.get_template_directory_uri().'/css/css-images/'.$tax_term->slug.'icon.png" alt="'.$tax_term->name.'">' . $tax_term->name . '</label></div>';
                            }
                        }
                        print '</select></div>';
                    }
                    
             */  
              ?>
               
					
					<div id="tabs" class="twelve columns alpha nomargin" style=" min-height:42px;padding-left:2px;">
					<input id="lease" class="home-search-selected-lease" type="Button" value="">
					<input id="sale" class="home-search-unselected-sale" type="Button" value="">
  <input type="hidden" id="tclick" name="filter_search_action" value=""/>
  


</div>
<div class="twelve columns alpha nomargin">
		
 <?php 	
                    if( !empty( $tax_terms_categ ) ){
						
                        print ' <div class="adv_search_checkers three columns alpha nomargin" style="float:left;padding:2px;">
						<select name="filter_search_type" id="search_'.$limit50.'" class="search_' . $limit50. ' cus-drop-icon"  
						style=" color:grey; height:43px;font-size:16px;-webkit-appearance: none;
-moz-appearance:    none;
appearance:         none;   
-webkit-border-radius: 0; 
-moz-border-radius: 0; 
border-radius: 0;padding-left:31px;border:none;-webkit-border:none;-moz-border:none;width:100%; 
text-transform:capitalize;">';
						
                        foreach ($tax_terms_categ as $categ) {
                            $icon_name          =   limit64( 'wp_estate_icon'.$categ->slug);
                            $limit50            =   limit50( $categ->slug);
   
                            print '<option value="'.$categ->name.'">'.$categ->name.'</option>';

                            if( $icons[$limit50]!='' ){
                                print'<img src="'.$icons[$limit50].'" alt="'.$categ->slug.'">' . $categ->name . '</label></div>';
                            }else{
                                print'<img src="'.get_template_directory_uri().'/css/css-images/'.$categ->slug.'icon.png" alt="'.$categ->name.'">' . $categ->name . '</label></div>';
                            }                      
                        }
                        print '</select></div>';
                    }
					?>		
					<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri()?>/css/city-autocomplete.css"/>
					<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?libraries=places&language=en"></script>
      <div class="seven columns alpha nomargin" style="padding:2px;"><input type="text" id="advanced_city1" name="advanced_city" class="form-control" autocomplete="off" data-country="us" placeholder="Enter City" 
	  style="float:left;height:43px;padding:0px 0px 0px 6px;border:none;width:99% !important; font-size:16px;
	  color:gray !important;" /></div>          
	  

<script src="<?php echo get_template_directory_uri()?>/js/jquery.city-autocomplete.js" type="text/javascript"></script>


                   <div class="two columns alpha nomargin" style="padding:2px;"> <input name="submit" type="submit" class="home-submit-btn" style="width:100%;" id="advanced_submit" value="<?php //_e('Search','wpestate');?>" ></div>
               
	  </div>
<?php                    $custom_advanced_search= get_option('wp_estate_custom_advanced_search','');
   
                    if ( $custom_advanced_search == 'yes'){
						
							?>
							

						
<?php                    }else{
                    ?>
                        
               
                
                 <div class="adv_search_internal">
                    <input type="text" id="adv_rooms_search" name="advanced_rooms" placeholder="<?php _e('Type Rooms No.','wpestate');?>"      class="advanced_select">
                    <input type="text" id="adv_bath_search"  name="advanced_bath"  placeholder="<?php _e('Type Bathrooms No.','wpestate');?>"  class="advanced_select">
                 </div>
            
                <div class="adv_search_internal lastadv">
                    <input type="text" id="price_low_search" name="price_low"  class="advanced_select" placeholder="<?php _e('Type Min. Price','wpestate');?>"/>
                    <input type="text" id="price_max_search" name="price_max"  class="advanced_select" placeholder="<?php _e('Type Max. Price','wpestate');?>"/>
                </div>
                
             
                
                
                <?php
                }
                ?>
                
               
                
                
                
                
                
                
                
       
     </form>
 <!-- end content-->
 </div>
</div>
</div>


 
 
</div>

<div id="wrapper" class="<?php print $options['fullwhite']; ?>">  

    <div class="<?php print $options['add_back']; ?>"></div>

    <?php
    //print breadcrumb_container($options['full_breadcrumbs'],$options['bread_align'] )
    ?>
    <div id="main" class="row <?php print $options['sidebar_status']; ?>" style="padding-top:40px;padding-bottom:0px;">
    <?php
    
    ?>

        
        <!-- begin content--> 
       
           
            
            <div class="inside_post inside_no_border <?php
            if (is_front_page()) {
                print 'is_home_page';
            }
            ?>" >
			<div style="width:100%;text-align: center;font-weight: bold;padding-top:10px;padding-bottom:20px">

</div>

<div class="four columns alpha myboxes" position="first">
<img src="<?php echo get_template_directory_uri();?>/images/Leasing.png"/>
</div>

<div class="four columns myboxes" position="second">
<img src="<?php echo get_template_directory_uri();?>/images/SellerNew.png"/>
</div>

<div class="four columns myboxes" position="third">
<img src="<?php echo get_template_directory_uri();?>/images/InvestorNew.png"/>
</div>

                <?php /*<?php while (have_posts()) : the_post(); ?>
                    
                    <?php the_content(); ?>


                    <?php endwhile; // end of the loop.  ?>
             <!-- end inside post-->
			 */?>
			


        <?php  //include(locate_template('customsidebar.php')); ?>
        
    </div><!-- #main -->    
</div><!-- #wrapper -->

<?php get_footer(); ?>
<style>
.myboxes {
	margin-bottom:0px;
}
</style>