<?php
$counter++;
$preview=array();
$preview[0]='';

$favorite_class='icon-fav-off';

if($curent_fav){
    if ( in_array ($post->ID,$curent_fav) ){
    $favorite_class='icon-fav-on';          
    } 
}
    

?>  
	  

<li>

<?php $price1 = intval( get_post_meta($post->ID, 'property_price', true) );
        if ($price1 != 0) {
           $price1 = number_format($price1);

           if ($where_currency == 'before') {
               $price1 = $currency . ' ' . $price1;
           } else {
               $price1 = $price1 . ' ' . $currency;
           }
        }else{
            $price1='';
        }
		?>
<div class="responsive-accordion-head twelve alpha nomargin" style="box-shadow: 0 1px 5px -2px rgba(0, 0, 0, 0.1);">
<div class="one columns alpha nomargin"><?php the_post_thumbnail('small_thumb');?></div>
<div class="two columns alpha nomargin" style="padding-top:12px;color:#0b8593 !important;">
<?php //the_title(); ?>

<?php 
echo $property_address       =   esc_html ( get_post_meta($post->ID, 'property_address', true) );
        //echo $property_city          =   get_the_term_list($post->ID, 'property_city', '', ', ', '') ;?>
</div>
<div class="five columns alpha nomargin" style="padding-left:8px;padding-top:12px;padding-bottom:12px;">
<?php echo get_the_term_list($post->ID, 'property_city', '', ', ', '') ;?></div>
<div class="two columns alpha nomargin" style="padding-left:15px;padding-top:12px;">
<?php echo get_the_term_list($post->ID, 'property_category', '', ', ', '');?></div>
<div class="two columns alpha nomargin" style="padding-top:10px;">
<?php echo $price1;?></div>
<div class="one columns alpha nomargin">
<i class="fa fa-chevron-down responsive-accordion-plus fa-fw"></i>
<i class="fa fa-chevron-up responsive-accordion-minus fa-fw"></i></div>

</div>

<div class="responsive-accordion-panel" style="display:inline-block;width:100%;margin-top:-10px;">
<div id="listing<?php print $counter;?>" class="property_listing twelve columns alpha nomargin">

<div class="three columns alpha nomargin"> 
        <?php
       
        if (has_post_thumbnail()):
           
        
            $preview = wp_get_attachment_image_src(get_post_thumbnail_id(), 'property_listings');
            $extra= array(
                'data-original'=>$preview[0],
                'class'	=> 'lazyload',    
            );
       
         
            $thumb_prop = get_the_post_thumbnail($post->ID, 'property_listings',$extra);
           
            $prop_stat = esc_html( get_post_meta($post->ID, 'property_status', true) );
            $featured  = intval  ( get_post_meta($post->ID, 'prop_featured', true) );
        
         
            //print '<figure data-link="' . get_permalink() . '">'. $thumb_prop;
			print '<figure><a href='.get_permalink().'>'. $thumb_prop.'</a>';
            
            if($featured==1){
                //print '<div class="featured_div">'.__('Featured','wpestate').'</div>';
            }
            
            if ($prop_stat != 'normal' && $prop_stat != '') {
                $ribbon_class = str_replace(' ', '-', $prop_stat);
                if (function_exists('icl_translate') ){
                    $prop_stat     =   icl_translate('wpestate','wp_estate_property_status'.$prop_stat, $prop_stat );
                }    
                print'<a href="' . get_permalink() . '"><div class="ribbon-wrapper-default ribbon-wrapper-' . $ribbon_class . '"><div class="ribbon-inside ' . $ribbon_class . '">' . $prop_stat . '</div></div></a>';         
                
            }
            
            /*print '         
                <figcaption data-link="' . get_permalink() . '">
                    <span class="fig-icon"></span>               
                </figcaption>              
            </figure>
            ';*/ 
			/*print '         
                <figcaption>
                    <span class="fig-icon"></span>               
                </figcaption>              
            </figure>
            ';*/
        endif;

        $price = intval( get_post_meta($post->ID, 'property_price', true) );
		$price = intval( get_post_meta($post->ID, 'property_price', true) );
        if ($price != 0) {
           $price = number_format($price);

           if ($where_currency == 'before') {
               $price = $currency . ' ' . $price;
           } else {
               $price = $price . ' ' . $currency;
           }
        }else{
            $price='';
        }
        
        $property_address       =   esc_html ( get_post_meta($post->ID, 'property_address', true) );
        $property_city          =   get_the_term_list($post->ID, 'property_city', '', ', ', '') ;
        $price_label            =   esc_html ( get_post_meta($post->ID, 'property_label', true) );
        $property_rooms         =   floatval  ( get_post_meta($post->ID, 'property_bedrooms', true) );
        $property_bathrooms     =   floatval  ( get_post_meta($post->ID, 'property_bathrooms', true) );
        $property_size          =   floatval  ( get_post_meta($post->ID, 'property_size', true) );
		$property_lot_size          =  floatval  ( get_post_meta($post->ID, 'property_lot_size', true) );
		$cap_rate          =  floatval  ( get_post_meta($post->ID, 'cap_rate', true) );
		$unit_price          =  floatval  ( get_post_meta($post->ID, 'unit_price', true) );
		$acre_price          =  floatval  ( get_post_meta($post->ID, 'acre_price', true) );
		
		
        $measure_sys            =   esc_html ( get_option('wp_estate_measure_sys','') ); 
       
        if($measure_sys== __('meters','wpestate') ){
            $measure_sys='m';
        }else{
             $measure_sys='ft';
        }
        ?>

		</div>
		
        <div class="property_listing_details four columns alpha nomargin">
		 <table cellpadding="0" cellspacing="0" border="0" style="background:#ffffff !important;border:none;">
		  <tr>
		   <td style="width:150px;">Title</td>
		   <td><h3 class="listing_title" style="margin-top:0px;"><a href="<?php echo the_permalink(); ?>" style="color:#000000;"><?php the_title(); ?></a> </h3></td>
		  </tr>
		  <tr>
		   <td>Address </td>
		   <td><div class="article_property_type" style="color:#000000 !important;"><?php  print $property_address.', '.$property_city; ?> </div></td>
		  </tr>
		  <tr>
		   <td> Sale Price</td>
		   <td><span class="property_price" style="color:#000000 !important;margin-bottom: 0px;font-weight: normal;"><?php print $price.' '.$price_label; ?></span> </td>
		  </tr>
		  
		  <tr>
		   <td>Type</td>
		   <td><div class="article_property_type" style="color:#000000 !important;">
                <?php echo get_the_term_list($post->ID, 'property_category', '', ', ', '');?> 
                <?php $action_list= get_the_term_list($post->ID, 'property_action_category', '', ', ', '');
                    if($action_list!=''){
                        //print __('in','wpestate').' '.$action_list;
						print __('','wpestate').' '.$action_list;
                    }
                ?>
            </div></td>
		  </tr>
		  <tr>
		   <td>Size</td>
		   <td><?php print $property_size;?></td>
		  </tr>
		 
		 </table>
            
            </div> 
<div class="property_listing_details three columns alpha nomargin">
  <table cellpadding="0" cellspacing="0" border="0" style="background:#ffffff !important;border:none;">
		  
		  <tr>
		   <td style="width:150px;">Lot Size</td>
		   <td><?php print $property_lot_size;?></td>
		  </tr>
		  <tr>
		   <td>Cap Rate</td>
		   <td><?php print $cap_rate;?></td>
		  </tr>
		  <tr>
		   <td>Price/Unit</td>
		   <td><?php print $unit_price;?></td>
		  </tr>
		  <tr>
		   <td>Price/Acre</td>
		   <td><?php print $acre_price;?></td>
		  </tr>
		 </table>
</div>		 
           
           
            
           
           <?php /* <div class="article_property_type listing_units">
                <?php   if($property_rooms!=0 ) {?>
                            <span class="inforoom">  <?php print $property_rooms;?></span>
                <?php   }
                        if($property_bathrooms!=0 ){
                ?>
                        <span class="infobath">  <?php print $property_bathrooms;?></span>
                <?php   } if($property_size!=0 ){ ?>
                        <span class="infosize"> <?php print $property_size.' '.$measure_sys.'<sup>2</sup>';?></span>
                <?php } ?>
            </div>*/?>
           
           
            
            
            <?php  
            if( !is_single() ){ 
                if(!isset($show_compare)){ ?>
                    <?php /*<div class="article_property_type">  
                        <a href="#" class="compare-action" data-pimage="<?php print $preview[0]; ?>" data-pid="<?php print $post->ID; ?>"> <span class="prop_plus">+</span> <?php _e('compare', 'wpestate'); ?></a>
                        <span class="icon-fav <?php echo $favorite_class;?>" data-postid="<?php echo $post->ID; ?>"></span>
                    </div>  */?>
                    
                <?php
                } else{
                    print '
                       <div class="article_property_type lastline">  
                          <span class="icon-fav icon-fav-on-remove" data-postid="'.$post->ID.'"> '.__('remove from favorites','wpestate').'</span>
                       </div>  
                    ';
                }
            }
            ?>
      
       

      
					</div>
					</div>
</li>

<style>
table tbody tr:nth-child(2n) {
    background: transparent !important;
}
table tbody tr td {
    padding: 5px 5px;
}
table tbody tr td:nth-child(1) {
    color:#a0a5a8 !important
}
table tbody tr td:nth-child(2) {
    color:#000000 !important
}
table tbody tr td:nth-child(2) a{
    color:#000000 !important
}
</style>

