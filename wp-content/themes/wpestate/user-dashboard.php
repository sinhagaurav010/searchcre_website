<?php
// Template Name: User Dashboard
// Wp Estate Pack
if ( !is_user_logged_in() ) {   
     wp_redirect(  home_url() );exit;
} 

global $current_user;
get_currentuserinfo();
$userID                         =   $current_user->ID;
$user_login                     =   $current_user->user_login;
$user_pack                      =   get_the_author_meta( 'package_id' , $userID );
$user_registered                =   get_the_author_meta( 'user_registered' , $userID );
$user_package_activation        =   get_the_author_meta( 'package_activation' , $userID );   
$paid_submission_status         =   esc_html ( get_option('wp_estate_paid_submission','') );
$price_submission               =   floatval( get_option('wp_estate_price_submission','') );
$submission_curency_status      =   esc_html( get_option('wp_estate_submission_curency','') );


$pages = get_pages(array(
        'meta_key' => '_wp_page_template',
        'meta_value' => 'user-dashboard-add.php'
        ));

if( $pages ){
    $edit_link = get_permalink( $pages[0]->ID);
}else{
    $edit_link=home_url();
}

$processor_link=get_procesor_link();


if( isset( $_GET['delete_id'] ) ) {
    if( !is_numeric($_GET['delete_id'] ) ){
          exit('you don\'t have the right to delete this');
    }else{
        $delete_id=$_GET['delete_id'];
        $the_post= get_post( $delete_id); 

        if( $current_user->ID != $the_post->post_author ) {
            exit('you don\'t have the right to delete this');;
        }else{
         
           
            // delete attchaments
            $arguments = array(
                'numberposts' => -1,
                'post_type' => 'attachment',
                'post_parent' => $delete_id,
                'post_status' => null,
                'exclude' => get_post_thumbnail_id(),
                'orderby' => 'menu_order',
                'order' => 'ASC'
            );
            $post_attachments = get_posts($arguments);
            
            foreach ($post_attachments as $attachment) {
               wp_delete_post($attachment->ID);                      
             }
           
            wp_delete_post( $delete_id ); 
         
        }  
        
    }
       
}  
  


get_header();
$options=sidebar_orientation($post->ID);
get_template_part('libs/templates/map-template');    
?> 
<!-- Google Map Code -->



<div id="wrapper" class="fullwhite">  
    <div class="<?php print $options['add_back']; ?>"></div>

    <?php
    print breadcrumb_container('yes',$options['bread_align'] )
    ?>
    <div id="main" class="row">
    <?php
    print display_breadcrumbs( 'yes' ,$options['bread_align_internal'] )
    ?>

        <!-- begin content--> 
        <div id="post" class=" noborder twelve columns alpha noshadow"> 
            <div class="inside_post inside_no_border submit_area">
                <?php if (esc_html( get_post_meta($post->ID, 'page_show_title', true) ) != 'no') { ?>
                     <h1 class="entry-title-agent-page"><?php the_title(); ?></h1>
                <?php } ?>

                <?php //get_template_part('libs/templates/user_menu'); 
               
               
                $paid_submission_status= esc_html ( get_option('wp_estate_paid_submission','') );
                if( $paid_submission_status == 'membership'){
                    get_pack_data_for_user_top($userID,$user_pack,$user_registered,$user_package_activation);
                }
                $prop_no =   intval( get_option('wp_estate_prop_no', '') ); 
                $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                
                $args = array(
                        'post_type'         => 'estate_property',
                        'author'            =>  $current_user->ID,
                        'paged'             => $paged,
                        'posts_per_page'    => $prop_no,
                        'post_status'       => array( 'any' )
                    );
               
              
                $prop_selection = new WP_Query($args);
                if( !$prop_selection->have_posts() ){
                    print '<h4>'.__('You don\'t have any properties yet!','wpestate').'</h4>';
                }
               
                while ($prop_selection->have_posts()): $prop_selection->the_post(); 
                    //  include(locate_template('dashboard-prop-listing.php'));
                    get_template_part('libs/templates/dashboard-prop-listing'); 
                endwhile;           
                ?>            
            </div> <!-- end inside post-->
            
            
                <?php   kriesi_pagination($prop_selection->max_num_pages, $range =2); ?>    
          
        </div>
        <!-- end content-->

       <?php //include(locate_template('customsidebar.php')); ?>

    </div><!-- #main -->    
</div><!-- #wrapper -->
<?php get_footer(); ?>