<?php
// Template Name: Blog list page
// Wp Estate Pack
get_header();
$options=sidebar_orientation($post->ID);
?>

<!-- Google Map Code -->
<?php 
get_template_part('libs/templates/map-template'); 
?> 
<!-- Google Map Code -->



<div id="wrapper" class="<?php print $options['fullwhite']; ?>">  
    <div class="<?php print $options['add_back']; ?>"></div>

    <?php
    print breadcrumb_container($options['full_breadcrumbs'],$options['bread_align'] )
    ?>
    <div id="main" class="row <?php print $options['sidebar_status']; ?>">
    <?php
    print display_breadcrumbs( $options['full_breadcrumbs'] ,$options['bread_align_internal'] )
    ?>



        <!-- begin content--> 
        <div id="post" class="blogborder <?php print $options['grid'].' '.$options['shadow']; ?>"> 
            <div class="inside_post inside_no_border inside_no_bottom" >
                <?php if (esc_html( get_post_meta($post->ID, 'page_show_title', true) ) == 'yes') { ?>
                    <h1 class="entry-title"><?php the_title(); ?></h1>
                <?php } ?>	   

                <?php
                 $paged = (get_query_var('paged')) ? get_query_var('paged') : 0;
                 $args = array(
                    'post_type' => 'post',
                    'paged' => $paged
                 );
                 
                 $blog_selection = new WP_Query($args);
                 $excerpt_class="";
                 while ($blog_selection->have_posts()): $blog_selection->the_post();
                    include(locate_template('bloglisting.php')); 
                 endwhile;
                 wp_reset_query();
                 ?>
                    
            </div> <!-- end inside post-->
            <?php kriesi_pagination($blog_selection->max_num_pages, $range = 2); ?>       
            <div class="spacer_archive"></div>

        </div>
        <!-- end content-->

       <?php  include(locate_template('customsidebar.php')); ?>


    </div><!-- #main -->    
</div><!-- #wrapper -->
<?php get_footer(); ?>