<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

<title><?php
        // Print the <title> tag based on what is being viewed
	global $page, $paged;
	wp_title( '|', true, 'right' );

	// Add the blog name.
	bloginfo( 'name' );

	// Add the blog description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		echo " | $site_description";

	// Add a page number if necessary:
	if ( $paged >= 2 || $page >= 2 )
		echo ' | ' . sprintf( __( 'Page %s', 'wpestate' ), max( $paged, $page ) );

	?>
</title>

<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
 


<?php 
$favicon        =   esc_html( get_option('wp_estate_favicon_image','') );
$geo_status     =   esc_html ( get_option('wp_estate_geolocation','') );

if ( $favicon!='' ){
    echo '<link rel="shortcut icon" href="'.$favicon.'" type="image/x-icon" />';
} else {
    echo '<link rel="shortcut icon" href="'.get_template_directory_uri().'/images/favicon.gif" type="image/x-icon" />';
}


wp_head();
?>
</head>


<body <?php body_class(); ?>>
<?php
global $current_user;
 $user_roles = $current_user->roles[0];
  ?>
  <div id="mySidenav" class="sidenav">
  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">
   <img src="<?php print get_template_directory_uri(); ?>/images/cancel.png" class="top-custom-bottom-link-img" alt="Cancel" <?php echo $header_margin;?> />
  </a>
  <?php if(is_user_logged_in() && $user_roles!='contributor'){
	  ?>
	  <?php
	  $current_user = wp_get_current_user();
	  //print_r($current_user);
	  if($current_user->user_firstname !=''){
	  echo "<div class='user-info-wel'>Welcome".$current_user->user_firstname .$current_user->user_lastname."</div>";
	  }else {
		  echo "<div class='user-info-wel'>Welcome : ".$current_user->user_login."</div>";
	  }
	  ?>
	  <a href="<?php echo get_home_url(); ?>/?page_id=5277" class="top-custom-link1">Profile</a>
	  <a href="<?php echo wp_logout_url( home_url() ); ?>" class="top-custom-link2">Logout</a>
	  <a href="<?php echo get_home_url(); ?>/?page_id=4841" class="top-custom-bottom-links">
	   <img src="<?php print get_template_directory_uri(); ?>/images/plus-button.png" class="top-custom-bottom-link-img" alt="icon" <?php echo $header_margin;?> /> Add Property</a>
	  <a href="<?php echo get_home_url(); ?>/?page_id=6722" class="top-custom-bottom-links">
	   <img src="<?php print get_template_directory_uri(); ?>/images/clipboard.png" class="top-custom-bottom-link-img" alt="icon" <?php echo $header_margin;?> /> Properties List</a>
	  <a href="<?php echo get_home_url(); ?>/?page_id=4690" class="top-custom-bottom-links">
	   <img src="<?php print get_template_directory_uri(); ?>/images/profile.png" class="top-custom-bottom-link-img" alt="icon" <?php echo $header_margin;?> /> Find a Broker</a>
	 
<?php   } elseif(is_user_logged_in() && $user_roles=='contributor') {?>
<?php 
	  $current_user = wp_get_current_user();
	  //print_r($current_user);
	  if($current_user->user_firstname !=''){
	  echo "<div class='user-info-wel'>Welcome : ".$current_user->user_firstname .' '.$current_user->user_lastname."</div>";
	  }else {
		  echo "<div class='user-info-wel'>Welcome : ".$current_user->user_login."</div>";
	  }
	  ?>
	  
 <a href="<?php echo get_home_url(); ?>/?page_id=5277" class="top-custom-link1">Profile</a>
 <a href="<?php echo wp_logout_url( home_url() ); ?>" class="top-custom-link2">Logout</a>
 <a href="<?php echo get_home_url(); ?>/?page_id=4841" class="top-custom-bottom-links">
 <img src="<?php print get_template_directory_uri(); ?>/images/plus-button.png" class="top-custom-bottom-link-img" alt="logo" <?php echo $header_margin;?> /> Add Property</a>
 <a href="<?php echo get_home_url(); ?>/?page_id=4839" class="top-custom-bottom-links">
 <img src="<?php print get_template_directory_uri(); ?>/images/block.png" class="top-custom-bottom-link-img" alt="logo" <?php echo $header_margin;?> /> My Properties</a>
 
	 
 

 <?php } else {?>
	  <a href="<?php echo get_home_url(); ?>/?page_id=6902" class="top-custom-link1">Login</a>
	   <a href="<?php echo get_home_url(); ?>/?page_id=6904" class="top-custom-link2">Register</a>
	     <a href="<?php echo get_home_url(); ?>/?page_id=4841" class="top-custom-bottom-links">
	   <img src="<?php print get_template_directory_uri(); ?>/images/plus-button.png" class="top-custom-bottom-link-img" alt="icon" <?php echo $header_margin;?> /> Add Property</a>
	   <a href="<?php echo get_home_url(); ?>/?page_id=6722" class="top-custom-bottom-links">
	   <img src="<?php print get_template_directory_uri(); ?>/images/clipboard.png" class="top-custom-bottom-link-img" alt="icon" <?php echo $header_margin;?> /> Properties List</a>
	   <a href="<?php echo get_home_url(); ?>/?page_id=4690" class="top-custom-bottom-links">
	   <img src="<?php print get_template_directory_uri(); ?>/images/profile.png" class="top-custom-bottom-link-img" alt="icon" <?php echo $header_margin;?> /> Find a Broker</a>
	 
	 
<?php }?>
 
</div>


<div id="page">

  <?php //if($user_roles!='subscriber'){
    //top_bar_front_end_menu(); 
  //}	
    $header_type_status     =   intval ( get_option('wp_estate_header_type',''));  ?>  
    <?php if(is_front_page()){?>
	<header id="" role="banner" class="branding_version_<?php echo $header_type_status;?>">
	<?php } else {?>
	 <header id="branding" role="banner" class="branding_version_<?php echo $header_type_status;?>">
        
        <?php   
	}
        
        $header_version="header_version_".$header_type_status;
        get_template_part('libs/templates/'.$header_version);
        ?>
       
      
        <?php 
        $geo_status     =   esc_html ( get_option('wp_estate_geolocation','') );
        $custom_image   =   '';
        $rev_slider     =   '';
        if( isset($post->ID) ){
            $custom_image   =   esc_html( esc_html(get_post_meta($post->ID, 'page_custom_image', true)) );  
            $rev_slider     =   esc_html( esc_html(get_post_meta($post->ID, 'rev_slider', true)) );
        }

        $show_adv_search_status     =   get_option('wp_estate_show_adv_search','');
        $hidden_class               =   ' geo_version_'.$header_type_status;
        
        if($geo_status=='yes' && $custom_image=='' && $rev_slider==''){
               if( get_post_type() === 'estate_property' && !is_tax() &&!is_search() ){
                   $hidden_class="geohide xhide";
               }
            
            ?>
        
            <div class="geolocation-button <?php print $hidden_class; ?>" id="geolocation-button"></div> 
            <div id="tooltip-geolocation"  class="tooltip-geolocation_<?php echo $header_type_status;?>"><?php _e('place me on the map','wpestate');?> </div>
           
            <?php  
           
            } 
       
            if( !is_page_template('contact-page.php') ){
                 if( $show_adv_search_status=='yes' or ( ($custom_image=='' && $rev_slider=='') ) ){
                      get_template_part('libs/templates/map-search-form');   
                 }         
            }
            
            ?>
    </header><!-- #branding -->
	<style>
	#advanced_search_map_button {
		display:none !important;
	}
	#search_map_button {
		display:none !important;
	}
	</style>
	<style>
.sidenav {
    height: 100%;
    width: 0;
    position: fixed;
    z-index: 999;
    top: 0;
    right: 0;
    background-color: #fff;
    overflow-x: hidden;
    transition: 0.5s;
    padding-top: 60px;
	box-shadow: -3px 0px 3px 1px #ccc;
}

.sidenav a {
    padding: 12px 8px 8px 10px;
    text-decoration: none;
    font-size: 16px;
    color: #818181;
    display: block;
    transition: 0.3s
}

.sidenav a:hover, .offcanvas a:focus{
    color: #000;
}

.closebtn {
    position: absolute;
    top: 0;
    left: 0px;
    font-size: 28px !important;
    margin-left: 0px;
	padding-left:10px;
	color:#999;
}
.closebtn img{
	height:30px !important;
}
@media screen and (max-height: 450px) {
  .sidenav {padding-top: 15px;}
  .sidenav a {font-size: 18px;}
}
</style>



